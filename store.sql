/*
 Navicat Premium Data Transfer

 Source Server         : LocalHost
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : store

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 12/03/2020 03:32:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bestsellers
-- ----------------------------
DROP TABLE IF EXISTS `bestsellers`;
CREATE TABLE `bestsellers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tab_text` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `main_product` int(11) NULL DEFAULT NULL,
  `index_position` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tab_text`(`tab_text`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bestsellers
-- ----------------------------
INSERT INTO `bestsellers` VALUES (1, 'Top 7', 24, 1, '2020-03-04 18:36:36', 1, NULL, NULL, 0);
INSERT INTO `bestsellers` VALUES (2, 'Mobiles & Tablets', 44, 1, '2020-03-05 22:32:21', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for bestsellersproduts
-- ----------------------------
DROP TABLE IF EXISTS `bestsellersproduts`;
CREATE TABLE `bestsellersproduts`  (
  `bestseller_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  INDEX `bestseller_id`(`bestseller_id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `bestsellersproduts_ibfk_1` FOREIGN KEY (`bestseller_id`) REFERENCES `bestsellers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bestsellersproduts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bestsellersproduts
-- ----------------------------
INSERT INTO `bestsellersproduts` VALUES (1, 19);
INSERT INTO `bestsellersproduts` VALUES (1, 23);
INSERT INTO `bestsellersproduts` VALUES (1, 38);
INSERT INTO `bestsellersproduts` VALUES (1, 41);
INSERT INTO `bestsellersproduts` VALUES (1, 57);
INSERT INTO `bestsellersproduts` VALUES (1, 69);
INSERT INTO `bestsellersproduts` VALUES (2, 36);
INSERT INTO `bestsellersproduts` VALUES (2, 8);
INSERT INTO `bestsellersproduts` VALUES (2, 7);
INSERT INTO `bestsellersproduts` VALUES (2, 45);
INSERT INTO `bestsellersproduts` VALUES (2, 43);
INSERT INTO `bestsellersproduts` VALUES (2, 29);

-- ----------------------------
-- Table structure for bigcarouselproducts
-- ----------------------------
DROP TABLE IF EXISTS `bigcarouselproducts`;
CREATE TABLE `bigcarouselproducts`  (
  `bigcarousel_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  INDEX `bigcarousel_id`(`bigcarousel_id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `bigcarouselproducts_ibfk_1` FOREIGN KEY (`bigcarousel_id`) REFERENCES `bigcarousels` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bigcarouselproducts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bigcarouselproducts
-- ----------------------------
INSERT INTO `bigcarouselproducts` VALUES (1, 1);
INSERT INTO `bigcarouselproducts` VALUES (1, 4);
INSERT INTO `bigcarouselproducts` VALUES (1, 5);
INSERT INTO `bigcarouselproducts` VALUES (1, 39);
INSERT INTO `bigcarouselproducts` VALUES (1, 35);
INSERT INTO `bigcarouselproducts` VALUES (1, 42);
INSERT INTO `bigcarouselproducts` VALUES (1, 43);
INSERT INTO `bigcarouselproducts` VALUES (1, 47);
INSERT INTO `bigcarouselproducts` VALUES (1, 48);
INSERT INTO `bigcarouselproducts` VALUES (1, 55);

-- ----------------------------
-- Table structure for bigcarousels
-- ----------------------------
DROP TABLE IF EXISTS `bigcarousels`;
CREATE TABLE `bigcarousels`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `main_img` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `index_position` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bigcarousels
-- ----------------------------
INSERT INTO `bigcarousels` VALUES (1, 'Mobile World', 'img/Banner/piece-banner-6-1.png', 1, '2020-03-04 15:28:33', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for carouselcategories
-- ----------------------------
DROP TABLE IF EXISTS `carouselcategories`;
CREATE TABLE `carouselcategories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NULL DEFAULT NULL,
  `index_position` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE,
  CONSTRAINT `carouselcategories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of carouselcategories
-- ----------------------------
INSERT INTO `carouselcategories` VALUES (1, 2, 5, '2020-03-04 15:29:27', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `img` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `view_times` int(5) NULL DEFAULT NULL,
  `department_id` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `department_id`(`department_id`) USING BTREE,
  CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Phones', 's', 0, 1, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (2, 'Phone Cases', '', 0, 1, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (3, 'Car Holder', '', 0, 2, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (4, 'Screwdriver Tool Kit', '', 0, 3, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (5, 'Connectors', '', 0, 4, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (6, 'Hubs', '', 0, 4, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (7, 'Mouse Cases', '', 0, 4, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (8, 'Converters', '', 0, 4, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (9, 'Pen Drive', '', 0, 4, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (10, 'Power Bank', '', 0, 1, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (11, 'Screen Protector', '', 0, 1, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (12, 'T-shirt', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (13, 'Jackets', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (14, 'Joggers Pants', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (15, 'Phone Holders', '', 0, 1, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (16, 'Bill Money Detector', '', 0, 7, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (17, 'Wall Holders', '', 0, 7, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (18, 'External Accesories', '', 0, 4, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (19, 'Adhensive Magnetic', '', 0, 1, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (20, 'Sets', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (21, 'Dress Shirt', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (22, 'Hoodie', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (23, 'Long Sleeve T-shirt ', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (24, 'Streetwear T-shirt', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (25, 'Sweater', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (26, 'Stickers', '', 0, 4, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (27, 'Harem Pants', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);
INSERT INTO `categories` VALUES (28, 'Pants', '', 0, 6, '2020-03-04 18:26:21', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for countryservices
-- ----------------------------
DROP TABLE IF EXISTS `countryservices`;
CREATE TABLE `countryservices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `shipping_price` decimal(5, 2) NULL DEFAULT NULL,
  `available` tinyint(1) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 246 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of countryservices
-- ----------------------------
INSERT INTO `countryservices` VALUES (1, 'Afghanistan ', 'AF', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (2, 'Aland Islands ', 'AX', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (3, 'Albania ', 'AL', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (4, 'Algeria ', 'DZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (5, 'American Samoa ', 'AS', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (6, 'Andorra ', 'AD', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (7, 'Angola ', 'AO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (8, 'Anguilla ', 'AI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (9, 'Antarctica ', 'AQ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (10, 'Antigua And Barbuda ', 'AG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (11, 'Argentina ', 'AR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (12, 'Armenia ', 'AM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (13, 'Aruba ', 'AW', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (14, 'Australia ', 'AU', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (15, 'Austria ', 'AT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (16, 'Azerbaijan ', 'AZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (17, 'Bahamas ', 'BS', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (18, 'Bahrain ', 'BH', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (19, 'Bangladesh ', 'BD', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (20, 'Barbados ', 'BB', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (21, 'Belarus ', 'BY', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (22, 'Belgium ', 'BE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (23, 'Belize ', 'BZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (24, 'Benin ', 'BJ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (25, 'Bermuda ', 'BM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (26, 'Bhutan ', 'BT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (27, 'Bolivia ', 'BO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (28, 'Bosnia And Herzegovina ', 'BA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (29, 'Botswana ', 'BW', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (30, 'Bouvet Island ', 'BV', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (31, 'Brazil ', 'BR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (32, 'British Indian Ocean Territory ', 'IO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (33, 'Brunei Darussalam ', 'BN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (34, 'Bulgaria ', 'BG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (35, 'Burkina Faso ', 'BF', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (36, 'Burundi ', 'BI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (37, 'Cambodia ', 'KH', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (38, 'Cameroon ', 'CM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (39, 'Canada ', 'CA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (40, 'Cape Verde ', 'CV', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (41, 'Cayman Islands ', 'KY', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (42, 'Central African Republic ', 'CF', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (43, 'Chad ', 'TD', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (44, 'Chile ', 'CL', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (45, 'China ', 'CN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (46, 'Christmas Island ', 'CX', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (47, 'Cocos (Keeling) Islands ', 'CC', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (48, 'Colombia ', 'CO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (49, 'Comoros ', 'KM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (50, 'Congo ', 'CG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (51, 'Congo, Democratic Republic ', 'CD', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (52, 'Cook Islands ', 'CK', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (53, 'Costa Rica ', 'CR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (54, 'Cote D\\ ', 'CI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (55, 'Croatia ', 'HR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (56, 'Cuba ', 'CU', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (57, 'Cyprus ', 'CY', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (58, 'Czech Republic ', 'CZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (59, 'Denmark ', 'DK', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (60, 'Djibouti ', 'DJ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (61, 'Dominica ', 'DM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (62, 'Dominican Republic ', 'DO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (63, 'Ecuador ', 'EC', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (64, 'Egypt ', 'EG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (65, 'El Salvador ', 'SV', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (66, 'Equatorial Guinea ', 'GQ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (67, 'Eritrea ', 'ER', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (68, 'Estonia ', 'EE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (69, 'Ethiopia ', 'ET', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (70, 'Falkland Islands (Malvinas) ', 'FK', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (71, 'Faroe Islands ', 'FO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (72, 'Fiji ', 'FJ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (73, 'Finland ', 'FI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (74, 'France ', 'FR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (75, 'French Guiana ', 'GF', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (76, 'French Polynesia ', 'PF', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (77, 'French Southern Territories ', 'TF', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (78, 'Gabon ', 'GA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (79, 'Gambia ', 'GM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (80, 'Georgia ', 'GE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (81, 'Germany ', 'DE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (82, 'Ghana ', 'GH', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (83, 'Gibraltar ', 'GI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (84, 'Greece ', 'GR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (85, 'Greenland ', 'GL', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (86, 'Grenada ', 'GD', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (87, 'Guadeloupe ', 'GP', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (88, 'Guam ', 'GU', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (89, 'Guatemala ', 'GT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (90, 'Guernsey ', 'GG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (91, 'Guinea ', 'GN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (92, 'Guinea-Bissau ', 'GW', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (93, 'Guyana ', 'GY', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (94, 'Haiti ', 'HT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (95, 'Heard Island & Mcdonald Islands ', 'HM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (96, 'Holy See (Vatican City State) ', 'VA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (97, 'Honduras ', 'HN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (98, 'Hong Kong ', 'HK', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (99, 'Hungary ', 'HU', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (100, 'Iceland ', 'IS', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (101, 'India ', 'IN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (102, 'Indonesia ', 'ID', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (103, 'Iran, Islamic Republic Of ', 'IR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (104, 'Iraq ', 'IQ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (105, 'Ireland ', 'IE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (106, 'Isle Of Man ', 'IM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (107, 'Israel ', 'IL', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (108, 'Italy ', 'IT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (109, 'Jamaica ', 'JM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (110, 'Japan ', 'JP', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (111, 'Jersey ', 'JE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (112, 'Jordan ', 'JO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (113, 'Kazakhstan ', 'KZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (114, 'Kenya ', 'KE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (115, 'Kiribati ', 'KI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (116, 'Korea ', 'KR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (117, 'Kuwait ', 'KW', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (118, 'Kyrgyzstan ', 'KG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (119, 'Lao People\\ ', 'LA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (120, 'Latvia ', 'LV', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (121, 'Lebanon ', 'LB', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (122, 'Lesotho ', 'LS', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (123, 'Liberia ', 'LR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (124, 'Libyan Arab Jamahiriya ', 'LY', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (125, 'Liechtenstein ', 'LI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (126, 'Lithuania ', 'LT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (127, 'Luxembourg ', 'LU', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (128, 'Macao ', 'MO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (129, 'Macedonia ', 'MK', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (130, 'Madagascar ', 'MG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (131, 'Malawi ', 'MW', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (132, 'Malaysia ', 'MY', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (133, 'Maldives ', 'MV', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (134, 'Mali ', 'ML', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (135, 'Malta ', 'MT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (136, 'Marshall Islands ', 'MH', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (137, 'Martinique ', 'MQ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (138, 'Mauritania ', 'MR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (139, 'Mauritius ', 'MU', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (140, 'Mayotte ', 'YT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (141, 'Mexico ', 'MX', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (142, 'Micronesia, Federated States Of ', 'FM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (143, 'Moldova ', 'MD', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (144, 'Monaco ', 'MC', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (145, 'Mongolia ', 'MN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (146, 'Montenegro ', 'ME', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (147, 'Montserrat ', 'MS', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (148, 'Morocco ', 'MA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (149, 'Mozambique ', 'MZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (150, 'Myanmar ', 'MM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (151, 'Namibia ', 'NA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (152, 'Nauru ', 'NR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (153, 'Nepal ', 'NP', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (154, 'Netherlands ', 'NL', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (155, 'Netherlands Antilles ', 'AN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (156, 'New Caledonia ', 'NC', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (157, 'New Zealand ', 'NZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (158, 'Nicaragua ', 'NI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (159, 'Niger ', 'NE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (160, 'Nigeria ', 'NG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (161, 'Niue ', 'NU', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (162, 'Norfolk Island ', 'NF', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (163, 'Northern Mariana Islands ', 'MP', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (164, 'Norway ', 'NO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (165, 'Oman ', 'OM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (166, 'Pakistan ', 'PK', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (167, 'Palau ', 'PW', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (168, 'Palestinian Territory, Occupied ', 'PS', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (169, 'Panama ', 'PA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (170, 'Papua New Guinea ', 'PG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (171, 'Paraguay ', 'PY', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (172, 'Peru ', 'PE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (173, 'Philippines ', 'PH', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (174, 'Pitcairn ', 'PN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (175, 'Poland ', 'PL', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (176, 'Portugal ', 'PT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (177, 'Puerto Rico ', 'PR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (178, 'Qatar ', 'QA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (179, 'Reunion ', 'RE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (180, 'Romania ', 'RO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (181, 'Russian Federation ', 'RU', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (182, 'Rwanda ', 'RW', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (183, 'Saint Barthelemy ', 'BL', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (184, 'Saint Helena ', 'SH', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (185, 'Saint Kitts And Nevis ', 'KN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (186, 'Saint Lucia ', 'LC', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (187, 'Saint Martin ', 'MF', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (188, 'Saint Pierre And Miquelon ', 'PM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (189, 'Saint Vincent And Grenadines ', 'VC', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (190, 'Samoa ', 'WS', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (191, 'San Marino ', 'SM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (192, 'Sao Tome And Principe ', 'ST', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (193, 'Saudi Arabia ', 'SA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (194, 'Senegal ', 'SN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (195, 'Serbia ', 'RS', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (196, 'Seychelles ', 'SC', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (197, 'Sierra Leone ', 'SL', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (198, 'Singapore ', 'SG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (199, 'Slovakia ', 'SK', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (200, 'Slovenia ', 'SI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (201, 'Solomon Islands ', 'SB', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (202, 'Somalia ', 'SO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (203, 'South Africa ', 'ZA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (204, 'South Georgia And Sandwich Isl. ', 'GS', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (205, 'Spain ', 'ES', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (206, 'Sri Lanka ', 'LK', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (207, 'Sudan ', 'SD', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (208, 'Suriname ', 'SR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (209, 'Svalbard And Jan Mayen ', 'SJ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (210, 'Swaziland ', 'SZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (211, 'Sweden ', 'SE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (212, 'Switzerland ', 'CH', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (213, 'Syrian Arab Republic ', 'SY', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (214, 'Taiwan ', 'TW', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (215, 'Tajikistan ', 'TJ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (216, 'Tanzania ', 'TZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (217, 'Thailand ', 'TH', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (218, 'Timor-Leste ', 'TL', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (219, 'Togo ', 'TG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (220, 'Tokelau ', 'TK', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (221, 'Tonga ', 'TO', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (222, 'Trinidad And Tobago ', 'TT', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (223, 'Tunisia ', 'TN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (224, 'Turkey ', 'TR', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (225, 'Turkmenistan ', 'TM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (226, 'Turks And Caicos Islands ', 'TC', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (227, 'Tuvalu ', 'TV', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (228, 'Uganda ', 'UG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (229, 'Ukraine ', 'UA', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (230, 'United Arab Emirates ', 'AE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (231, 'United Kingdom ', 'GB', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (232, 'United States ', 'US', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (233, 'United States Outlying Islands ', 'UM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (234, 'Uruguay ', 'UY', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (235, 'Uzbekistan ', 'UZ', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (236, 'Vanuatu ', 'VU', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (237, 'Venezuela ', 'VE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (238, 'Viet Nam ', 'VN', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (239, 'Virgin Islands, British ', 'VG', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (240, 'Virgin Islands, U.S. ', 'VI', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (241, 'Wallis And Futuna ', 'WF', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (242, 'Western Sahara ', 'EH', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (243, 'Yemen ', 'YE', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (244, 'Zambia ', 'ZM', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);
INSERT INTO `countryservices` VALUES (245, 'Zimbabwe ', 'ZW', 50.00, 0, '2020-03-04 18:30:11', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `img` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `view_times` int(5) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  UNIQUE INDEX `img`(`img`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES (1, 'Mobiles & Tablets', 'img/Departments/Mobiles_Tablets.png', 0, '2020-03-06 02:22:40', 1, NULL, NULL, 0);
INSERT INTO `departments` VALUES (2, 'Car, Motorbike & Accesories', 'img/Departments/Car_Motorbike_Accesories.png', 0, '2020-03-06 02:22:40', 1, NULL, NULL, 0);
INSERT INTO `departments` VALUES (3, 'Tools & Equipment', 'img/Departments/Tools_Equipment.png', 0, '2020-03-06 02:22:40', 1, NULL, NULL, 0);
INSERT INTO `departments` VALUES (4, 'Computer & Accesories', 'img/Departments/Computer_Accesories.png', 0, '2020-03-06 02:22:40', 1, NULL, NULL, 0);
INSERT INTO `departments` VALUES (5, 'Women\'s Clothes', 'img/Departments/Womens_Clothes.png', 0, '2020-03-06 02:22:40', 1, NULL, NULL, 0);
INSERT INTO `departments` VALUES (6, 'Men\'s Clothes', 'img/Departments/Mens_Clothes.png', 0, '2020-03-06 02:22:40', 1, NULL, NULL, 0);
INSERT INTO `departments` VALUES (7, 'Office Supplies', 'img/Departments/Office_Supplies.png', 0, '2020-03-06 02:22:40', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for indexcategoryproducts
-- ----------------------------
DROP TABLE IF EXISTS `indexcategoryproducts`;
CREATE TABLE `indexcategoryproducts`  (
  `indexcategory_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  INDEX `indexcategory_id`(`indexcategory_id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `indexcategoryproducts_ibfk_1` FOREIGN KEY (`indexcategory_id`) REFERENCES `carouselcategories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `indexcategoryproducts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of indexcategoryproducts
-- ----------------------------

-- ----------------------------
-- Table structure for indexproducts
-- ----------------------------
DROP TABLE IF EXISTS `indexproducts`;
CREATE TABLE `indexproducts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `link` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `grill_size` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of indexproducts
-- ----------------------------

-- ----------------------------
-- Table structure for listcharacteristics
-- ----------------------------
DROP TABLE IF EXISTS `listcharacteristics`;
CREATE TABLE `listcharacteristics`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `characteristic` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `listcharacteristics_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of listcharacteristics
-- ----------------------------

-- ----------------------------
-- Table structure for minipromos
-- ----------------------------
DROP TABLE IF EXISTS `minipromos`;
CREATE TABLE `minipromos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `strong` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `img` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `size` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `link` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `button_text` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `index_position` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of minipromos
-- ----------------------------
INSERT INTO `minipromos` VALUES (1, 'All the cases you need', 'In one place', 'img/Banner/banner-4.jpg', 'col-xs-12 col-sm-6', 'Products/Phone%20Cases', 'Shop Now', 3, '2020-03-04 15:22:25', 1, NULL, NULL, 0);
INSERT INTO `minipromos` VALUES (2, 'All kind of Pen drive', 'Get the best', 'img/Banner/banner-5.jpg', 'col-xs-12 col-sm-6', 'Products/Pen%20Drive', 'Choose one', 3, '2020-03-04 15:22:25', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for orderproducts
-- ----------------------------
DROP TABLE IF EXISTS `orderproducts`;
CREATE TABLE `orderproducts`  (
  `order_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  INDEX `order_id`(`order_id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `orderproducts_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orderproducts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orderproducts
-- ----------------------------

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_address` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `estimated_arrive` datetime(0) NULL DEFAULT NULL,
  `total_amount` decimal(8, 2) NOT NULL,
  `created_code` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`status`) REFERENCES `status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for owlcarousels
-- ----------------------------
DROP TABLE IF EXISTS `owlcarousels`;
CREATE TABLE `owlcarousels`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `subtitle` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `strong` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `img` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `link` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `button_text` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `index_position` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of owlcarousels
-- ----------------------------
INSERT INTO `owlcarousels` VALUES (1, 'Timepieces that make a statement up to', 'shop to get what you loves', '40% Off', 'img/banner/banner-1.jpg', 'products', 'Shop Now', 4, '2020-03-04 18:32:04', 1, NULL, NULL, 0);
INSERT INTO `owlcarousels` VALUES (2, 'Timepieces that make a statement up to', 'shop to get what you loves', '40% Off', 'img/banner/banner-2.jpg', 'products', 'Shop Now', 4, '2020-03-04 18:32:04', 1, NULL, NULL, 0);
INSERT INTO `owlcarousels` VALUES (3, 'Timepieces that make a statement up to', 'shop to get what you loves', '40% Off', 'img/banner/banner-3.jpg', 'products', 'Shop Now', 4, '2020-03-04 18:32:04', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for productimages
-- ----------------------------
DROP TABLE IF EXISTS `productimages`;
CREATE TABLE `productimages`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `productimages_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of productimages
-- ----------------------------

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `provider_price` decimal(5, 2) NOT NULL,
  `price` decimal(5, 2) NOT NULL,
  `discount_price` decimal(5, 2) NULL DEFAULT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cover_image` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `star_rating` decimal(5, 1) NULL DEFAULT NULL,
  `shipping_days` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `shipping_price` decimal(5, 2) NULL DEFAULT NULL,
  `link_product` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `sold_times` int(11) NULL DEFAULT NULL,
  `provider_id` int(11) NULL DEFAULT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  UNIQUE INDEX `link_product`(`link_product`) USING BTREE,
  INDEX `provider_id`(`provider_id`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE,
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `products_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 112 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'Metal Magnetic Adsorption Case For iPhone', 9.50, 99.00, 129.50, 'Eqvvol Metal Magnetic Adsorption Case For iPhone XS MAX X XR 8 7 Plus 6 6s Case Double Sided Glass Magnet Case Cover 7Plus Funda', 'img/Products/Product 1/cover.jpg', 58.9, '45', 1.00, 'https://www.aliexpress.com/item/Eqvvol-Metal-Magnetic-Adsorption-Case-For-iPhone-XS-MAX-X-XR-8-7-Plus-6-6s/33000684770.html?spm=2114.search0604.3.78.7dde7e84ZM80qM&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=8df4730a-b031-4fb8-a392-f4174bf97419-10&algo_pvid=8df4730a-b031-4fb8-a392-f4174bf97419&transAbTest=ae803_3', 0, 1, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (2, 'Magnetic Adsorption Case for Huawei P30 P30 Pro', 7.01, 1.00, 0.00, 'Hadinas Magnetic Adsorption Case for Huawei P30 P30 Pro Magnet Gradient Glass Phone Case for Huawei p30 pro p 30 p30pro Coque', 'img/Products/Product 2/cover.jpg', 48.9, '42', 11.00, 'https://www.aliexpress.com/item/Hadinas-Magnetic-Adsorption-Case-for-Huawei-P30-P30-Pro-Magnet-Gradient-Glass-Phone-Case-for-Huawei/32996184076.html?spm=2114.search0604.3.54.7dde7e84ZM80qM&s=p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=8df4730a-b031-4fb8-a392-f4174bf97419-7&algo_pvid=8df4730a-b031-4fb8-a392-f4174bf97419&transAbTe', 0, 2, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (3, 'Magnetic Adsorption Metal Case For Samsung Galaxy S9 S8 Plus S7 Edge', 5.39, 1.00, 0.00, 'Eqvvol Magnetic Adsorption Metal Case For Samsung Galaxy S9 S8 Plus S7 Edge Tempered Glass Back Magnet Cover For Note 8 9 Cases', 'img/Products/Product 3/cover.jpg', 48.9, '60', 1.00, 'https://www.aliexpress.com/item/Eqvvol-Magnetic-Adsorption-Metal-Case-For-Samsung-Galaxy-S9-S8-Plus-S7-Edge-Tempered-Glass-Back/32968157493.html?spm=2114.search0604.3.62.7dde7e84ZM80qM&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=8df4730a-b031-4fb8-a392-f4174bf97419-8&algo_pvid=8df4730a-b031-4fb8-a392-f4174bf97419&transAbTest=ae803', 0, 1, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (4, 'Magnetic Adsorption Metal Case For Samsung Galaxy', 4.40, 1.00, 0.00, 'Magnetic Adsorption Metal Case For Samsung Galaxy S8 S9 S10 Plus S10E S7 Edge Note 8 9 M20 M10 A30 A50 A7 A8 A9 J4 J6 Plus 2018', 'img/Products/Product 4/cover.png', 48.9, '60', 1.00, 'https://www.aliexpress.com/item/Magnetic-Adsorption-Metal-Case-For-Samsung-Galaxy-S8-S9-S10-Plus-S10E-S7-Edge-Note-8/33007129906.html?spm=2114.search0604.3.78.38cb3337ykh5e7&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=8c2b40ff-a6ff-43d9-967a-687d47c3f3b9-10&algo_pvid=8c2b40ff-a6ff-43d9-967a-687d47c3f3b9&transAbTest=ae803_3', 0, 3, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (5, '3D Cartoon Soft Silicone Phone Case For Samsung Galaxy A8', 1.99, 1.00, 0.00, 'JURCHEN 6.3\" TPU Cover For Samsung Galaxy A8 Star Case 3D Cartoon Soft Silicone Phone Case For Samsung Galaxy A8 Star Cover Case', 'img/Products/Product 5/cover.jpg', 48.9, '42', 1.00, 'https://www.aliexpress.com/item/JURCHEN-6-3-TPU-Cover-For-Samsung-Galaxy-A8-Star-Case-3D-Cartoon-Soft-Silicone-Phone/32943949523.html?spm=2114.search0604.3.45.c0c13e7ct0N8oC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=07d7b2e6-b350-4de2-8784-980db99abe6d-6&algo_pvid=07d7b2e6-b350-4de2-8784-980db99abe6d&transAbTest=ae803_3', 0, 4, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (6, 'Case For Huawei Mate 20 Lite', 3.99, 1.00, 0.00, 'Case For Huawei Mate 20 Lite Case Soft Fabric TPU Silicone Cover Ultra-thin Shockproof Bumper Back Cover For Huawei Mate 20 Lite', 'img/Products/Product 6/cover.jpg', 48.9, '60', 1.00, 'https://www.aliexpress.com/item/Case-For-Huawei-Mate-20-Lite-Case-Soft-Fabric-TPU-Silicone-Cover-Ultra-thin-Shockproof-Bumper/32924589761.html?spm=2114.search0604.3.171.1e103249Z60r4W&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=f3dcf799-0c83-4bbd-a40f-66204ffd4452-21&algo_pvid=f3dcf799-0c83-4bbd-a40f-66204ffd4452&transAbTest=ae803', 0, 5, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (7, 'Bumper Case For Huawei Mate 20 Lite', 1.98, 1.00, 0.00, 'Bumper Case For Huawei Mate 20 Lite Case Luxury Transparent TPU Silicone Plating Shining Cover For Huawei Mate 20 lite Case', 'img/Products/Product 7/cover.jpg', 100.0, '32', 1.00, 'https://www.aliexpress.com/item/Bumper-Case-For-Huawei-Mate-20-Lite-Case-Luxury-Transparent-TPU-Silicone-Plating-Shining-Cover-For/32930672987.html?spm=2114.search0604.3.73.2bfc3249ChbjKo&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=8c856ffc-632c-4c23-b492-9eed3e7ced6c-9&algo_pvid=8c856ffc-632c-4c23-b492-9eed3e7ced6c&transAbTest=ae', 0, 6, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (8, 'Huawei Mate 20 Lite Case Tempered Glass Planet Space', 4.99, 1.00, 0.00, 'For Huawei Mate 20 Lite Case Tempered Glass Planet Space Cover Glass Back Case for Huawei Mate20 Lite Coque Shell Mate 20 Lite', 'img/Products/Product 8/cover.jpg', 85.7, '32', 1.00, 'https://www.aliexpress.com/item/For-Huawei-Mate-20-Lite-Case-Tempered-Glass-Planet-Space-Cover-Glass-Back-Case-for-Huawei/32935166283.html?spm=2114.search0604.3.115.1e103249Z60r4W&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=f3dcf799-0c83-4bbd-a40f-66204ffd4452-14&algo_pvid=f3dcf799-0c83-4bbd-a40f-66204ffd4452&transAbTest=ae803_3', 0, 7, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (9, ' VGA to HDMI Adapter Mini Converter', 5.27, 1.00, 0.00, 'Rankman VGA to HDMI Adapter Mini Converter with Audio VGA HDMI 1080p for Projector PC HDTV Monitor TV-box', 'img/Products/Product 9/cover.jpg', 48.9, '31', 11.00, 'https://www.aliexpress.com/item/Rankman-VGA-to-HDMI-Adapter-Mini-Converter-with-Audio-VGA-HDMI-1080p-for-Projector-PC-HDTV/32855108075.html?spm=2114.search0604.3.6.55a83f8b6byWBX&s=p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=538dfed7-3e87-4cd1-849d-7e1a2f7becde-3&algo_pvid=538dfed7-3e87-4cd1-849d-7e1a2f7becde&transAbTest=ae803_3', 0, 8, 8, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (10, 'USB Hub 3.0 Multi USB 3.0 Hub', 6.24, 11.00, 0.00, 'USB Hub 3.0 Multi USB 3.0 Hub USB Splitter High Speed 3 / 6 Ports Hab TF SD Card Reader All In One For PC Computer Accessories', 'img/Products/Product 10/cover.jpg', 48.9, '20 - 38', 1.00, 'https://www.aliexpress.com/item/USB-Hub-Combo-6-Ports-2-0-Micro-Card-Reader-SD-TF-High-Speed-Multi-USB/32808029308.html?spm=2114.search0604.3.44.55a83f8b6byWBX&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=538dfed7-3e87-4cd1-849d-7e1a2f7becde-8&algo_pvid=538dfed7-3e87-4cd1-849d-7e1a2f7becde&transAbTest=ae803_3', 0, 9, 6, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (11, 'Micro USB Hub 2.0 Multi USB Port 4/7 Ports Hub', 6.97, 1.00, 0.00, 'Micro USB Hub 2.0 Multi USB Port 4/7 Ports Hub USB High Speed Hab With on/off Switch USB Splitter For PC Computer Accessories', 'img/Products/Product 11/cover.jpg', 48.9, '20 - 38', 1.00, 'https://www.aliexpress.com/item/Micro-USB-Hub-2-0-Multi-USB-Port-4-7-Ports-Hub-USB-High-Speed-Hab/32955096863.html?spm=2114.search0604.3.52.55a83f8b6byWBX&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=538dfed7-3e87-4cd1-849d-7e1a2f7becde-9&algo_pvid=538dfed7-3e87-4cd1-849d-7e1a2f7becde&transAbTest=ae803_3', 0, 10, 6, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (12, 'Baseus Multi USB 3.0 / Type', 17.98, 1.00, 0.00, 'Baseus Multi USB 3.0 / Type C HUB to USB3.0 + 3 USB2.0 for Macbook Pro HUB Adapter for Huawei P20 Computer Hard Drive Accessory', 'img/Products/Product 12/cover.jpg', 48.9, '33', 1.00, 'https://www.aliexpress.com/item/Baseus-Multi-USB-3-0-Type-C-HUB-to-USB3-0-3-USB2-0-for-Macbook/32950002898.html?spm=2114.search0604.3.60.55a83f8b6byWBX&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=538dfed7-3e87-4cd1-849d-7e1a2f7becde-10&algo_pvid=538dfed7-3e87-4cd1-849d-7e1a2f7becde&transAbTest=ae803_3', 0, 11, 6, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (13, 'Baseus USB Type C HUB to 3.0 HDMI HUB with Wireless Charge ', 51.41, 1.00, 0.00, 'Baseus USB Type C HUB to 3.0 HDMI HUB with Wireless Charge for MacBook Pro Multi USB HUB Computer Accessories Splitter USB C HUB', 'img/Products/Product 13/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Baseus-USB-Type-C-HUB-to-3-0-HDMI-HUB-with-Wireless-Charge-for-MacBook-Pro/32964899084.html?spm=2114.search0604.3.243.55a83f8b6byWBX&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=538dfed7-3e87-4cd1-849d-7e1a2f7becde-34&algo_pvid=538dfed7-3e87-4cd1-849d-7e1a2f7becde&transAbTest=ae803_3', 0, 12, 6, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (14, 'llano 8 in 1 usb Hub Splitter Power Video SD / TF Card Reader USB HUB', 8.81, 1.00, 0.00, 'llano 8 in 1 usb Hub Splitter Power Video SD / TF Card Reader USB HUB for MacBook Pro Hub Splitter 6-Port USB 2.0 Hub Converter', 'img/Products/Product 14/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/llano-8-in-1-usb-Hub-Splitter-Power-Video-SD-TF-Card-Reader-USB-HUB-for/32844977160.html?spm=2114.10010108.1000016.18.cf9c3b00et29D7&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=065aad0d-a2b2-4172-a577-1d62770dc44d', 0, 13, 6, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (15, 'EastVita Multifunctional 10 Ports USB Hub Splitter Adapter USB Charger with Wireless Charging', 22.00, 1.00, 0.00, 'EastVita Multifunctional 10 Ports USB Hub Splitter Adapter USB Charger with Wireless Charging Practical Tool r20', 'img/Products/Product 15/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/EastVita-Multifunctional-10-Ports-USB-Hub-Splitter-Adapter-USB-Charger-with-Wireless-Charging-Practical-Tool-r20/32863680893.html?spm=2114.10010108.1000016.19.cf9c3b00et29D7&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=065aad0d-a2b2-4172-a577-1d62770dc44d', 0, 14, 6, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (16, 'Baseus USB Type C HUB Docking Station', 46.14, 1.00, 0.00, 'Baseus USB Type C HUB Docking Station For Samsung S10 S9 Dex Pad Station USB-C to HDMI Dock Power Adapter For Huawei P30 P20 Pro', 'img/Products/Product 16/cover.jpg', 48.9, '20 - 38', 1.00, 'https://www.aliexpress.com/item/Baseus-USB-Type-C-HUB-Docking-Station-For-Samsung-S10-S9-Dex-Pad-Station-USB-C/32995822208.html?spm=2114.10010108.1000016.24.cf9c3b00et29D7&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=065aad0d-a2b2-4172-a577-1d62770dc44d', 0, 15, 8, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (17, 'EECPT HDMI to VGA Adapter Converter 1080P', 6.49, 1.00, 0.00, 'EECPT HDMI to VGA Adapter Converter 1080P Digital to Analog Video Audio Adaptor for PS4 TV box Laptop to PC Screen TV Projector', 'img/Products/Product 17/cover.jpg', 48.9, '31', 1.00, 'https://www.aliexpress.com/item/EECPT-HDMI-to-VGA-Adapter-Converter-1080P-Digital-to-Analog-Video-Audio-Adaptor-for-PS4-TV/33004746813.html?spm=2114.10010108.1000016.9.558a1bf3iJqPzE&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=7e72a32e-d971-4c45-bd03-a014cb2e4a1d', 0, 16, 8, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (18, 'TISHRIC HDMI TO VGA Adapter HDMI Cable', 2.38, 1.00, 0.00, 'TISHRIC HDMI TO VGA Adapter HDMI Cable 1080P Digital TO Analog Audio Converter Male To Famale For PC Laptop TV Box Projector', 'img/Products/Product 18/cover.jpg', 48.9, '42', 11.00, 'https://www.aliexpress.com/item/TISHRIC-HDMI-TO-VGA-Adapter-HDMI-Cable-1080P-Digital-TO-Analog-Audio-Converter-Male-To-Famale/32969902307.html?spm=2114.10010108.1000016.14.70c16c90EyL0Gn&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=92ec9d72-6bed-469e-8f18-89b19e0b4669', 0, 17, 8, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (19, 'Portable Desktop Counterfeit Bill Detector Cash', 14.99, 1.00, 0.00, 'Portable Desktop Counterfeit Bill Detector Cash Currency Banknotes Notes Checker Support Ultraviolet UV and Magnifier EU Plug', 'img/Products/Product 19/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Portable-Desktop-Counterfeit-Bill-Detector-Cash-Currency-Banknotes-Notes-Checker-Support-Ultraviolet-UV-and-Magnifier-EU/32969930998.html?spm=2114.search0604.3.23.331e18e5OHJ2ic&s=p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=43507dc1-d20b-49cb-afc8-ca21585fa1b4-5&algo_pvid=43507dc1-d20b-49cb-afc8-c', 0, 18, 16, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (20, 'Computer Wireless Mouse Case', 4.99, 1.00, 0.00, 'Computer Wireless Mouse Case For Logitech Inalambrico Bluetooth MX Master/Master 2S , EVA Carrying Pouch Cover Bag Compact Sizes', 'img/Products/Product 20/cover.jpg', 48.9, '34', 1.00, 'https://www.aliexpress.com/item/Computer-Wireless-Mouse-Case-For-Logitech-Inalambrico-Bluetooth-MX-Master-Master-2S-EVA-Carrying-Pouch-Cover/32977630395.html?spm=2114.search0604.6.49.7dc02e34dPyxeq&s=p', 0, 19, 7, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (21, 'Portable Travel Durable Case', 3.89, 1.00, 0.00, 'Portable Travel Durable Case For Logitech Wireless Mouse M185 Logitech M186 Pouch Cover Case Bag', 'img/Products/Product 21/cover.jpg', 48.9, '42', 1.00, 'https://www.aliexpress.com/item/Portable-Travel-Durable-Case-For-Logitech-Wireless-Mouse-M185-Logitech-M186-Pouch-Cover-Case-Bag/32961591461.html?spm=2114.10010108.1000016.12.45786031C0FPp0&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=2a27b6e5-01c6-487c-b7d9-dfd44f59388c', 0, 20, 7, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (22, 'Baseus 4Pcs Car Phone Holder', 4.99, 11.00, 0.00, 'Baseus 4Pcs Car Phone Holder Suction Cup Sucker Self Adhesive Wall Hooks Hanger For Car Interior Home Storage Organizer', 'img/Products/Product 22/cover.jpg', 48.9, '42', 1.00, 'https://www.aliexpress.com/item/Baseus-4Pcs-Car-Phone-Holder-Suction-Cup-Sucker-Self-Adhesive-Wall-Hooks-Hanger-For-Car-Interior/32977757447.html?spm=2114.10010108.1000016.20.45786031C0FPp0&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=2a27b6e5-01c6-487c-b7d9-dfd44f59388c', 0, 11, 17, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (23, 'ORICO Mini USB 3.0 HUB 4 Port USB Splitter Power Adapter', 13.82, 1.00, 0.00, 'ORICO Mini USB 3.0 HUB 4 Port USB Splitter Power Adapter for iMac/Windows XP/Vista/7/8 Computer Laptop Accessories HUB USB 3.0', 'img/Products/Product 23/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/ORICO-Mini-USB-3-0-HUB-4-Port-USB-Splitter-Power-Adapter-for-iMac-Windows-XP/32926338867.html?spm=2114.search0604.3.121.61a730ddAIR13p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=eb932de6-fd8e-48c9-8d2c-9183e7b2ac4a-20&algo_pvid=eb932de6-fd8e-48c9-8d2c-9183e7b2ac4a&transAbTest=ae803_3', 0, 21, 6, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (24, 'ORICO USB HUB with 4 port USB 3.0 and USB 2.0 Aluminum Spliter portable HUB', 21.32, 45.21, 74.32, 'ORICO USB HUB with 4 port USB 3.0 and USB 2.0 Aluminum Spliter portable HUB For Laptop computer Accessories With 1m Data cable', 'img/Products/Product 24/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/32835374400.html?spm=a2g0o.detail.1000016.1.ef63670a7Zi4zN&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=8971d325-30d9-489b-88fc-991905574efb', 0, 22, 6, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (25, '9.5mm DVD/CD-ROM RW Case DVD Player Drive Enclosure USB 3.0', 19.30, 1.00, 0.00, '9.5mm DVD/CD-ROM RW Case DVD Player Drive Enclosure USB 3.0 SATA External Portable DVD Enclosure for Macbook PC Laptop', 'img/Products/Product 25/cover.png', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/9-5mm-DVD-CD-ROM-RW-Case-DVD-Player-Drive-Enclosure-USB-3-0-SATA-External/32985471132.html?af=607243&aff_platform=aaf&cpt=1560565811698&afref=&dp=02db62855ee4b06456a41544ec97d1be&cv=47843&pvid=fbbcf8e0-552a-4999-a1e4-296668c7c934&mall_affr=pr3&sk=VnYZvQVf&aff_trace_key=6c9df12a97be4296839474bf1c742b90-1560565811698-09894-VnYZvQVf&rmsg=replace_same_item&scm=1007.23534.124736.0&terminal_id=b9b0f8850fcc49b18968b5b5800e7f3c', 0, 23, 18, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (26, 'Aluminum Alloy 3 Ports USB 3.0 Hub With VGA Output Micro USB Port Multi USB', 27.56, 1.00, 0.00, 'Aluminum Alloy 3 Ports USB 3.0 Hub With VGA Output Micro USB Port Multi USB Splitter Adapter For Macbook Computer Accessories', 'img/Products/Product 26/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Aluminum-Alloy-3-Ports-USB-3-0-Hub-With-VGA-Output-Micro-USB-Port-Multi-USB/33034914926.html?spm=2114.search0604.3.100.294f5d76dx9FfZ&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=d0657b1f-be4a-459c-bf95-cec1ba7c15e5-17&algo_pvid=d0657b1f-be4a-459c-bf95-cec1ba7c15e5&transAbTest=ae803_3', 0, 24, 9, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (27, 'Ouhaobin High Speed Card Reader Adapter', 39.65, 1.00, 0.00, 'Ouhaobin High Speed Card Reader Adapter 4 In 1 With 8/16/32/64/128 GB TF Flash SD Card Readers Computer Accessories Hot Selling', 'img/Products/Product 27/cover.jpg', 48.9, '13', 1.00, 'https://www.aliexpress.com/item/Ouhaobin-High-Speed-Card-Reader-Adapter-4-In-1-With-8-16-32-64-128-GB/33022453609.html?spm=2114.search0604.3.89.6fcf2a013AVoRD&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=2356aa9f-d4fc-478e-96a4-c90d5d0fd6f3-15&algo_pvid=2356aa9f-d4fc-478e-96a4-c90d5d0fd6f3&transAbTest=ae803_3', 0, 25, 9, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (28, 'Portable Power Bank', 24.90, 1.00, 0.00, 'Portable Power Bank 8000mAh Fast Charing for mobile phone', 'img/Products/Product 28/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Portable-Power-Bank-8000mAh-Fast-Charing-for-mobile-phone/33034796593.html?spm=2114.search0603.3.50.7a9bda32dIbQQQ&s=p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=06554b33-13bb-4b8c-9178-3e76571b9c13-6&algo_pvid=06554b33-13bb-4b8c-9178-3e76571b9c13&transAbTest=ae803_3', 0, 26, 10, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (29, 'Power Bank', 29.98, 1.00, 0.00, 'Power Bank for xiaomi mi iPhone,USAMS Mini Pover Bank 10000mAh LED Display Powerbank External Battery Poverbank Fast charging', 'img/Products/Product 29/cover.jpg', 48.9, '20 - 38', 1.00, 'https://www.aliexpress.com/item/Power-Bank-for-xiaomi-mi-iPhone-USAMS-Mini-Pover-Bank-10000mAh-LED-Display-Powerbank-External-Battery/32955585679.html?spm=2114.search0603.3.58.7a9bda32dIbQQQ&s=p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=06554b33-13bb-4b8c-9178-3e76571b9c13-7&algo_pvid=06554b33-13bb-4b8c-9178-3e76571b9c13&transAb', 0, 27, 10, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (30, '3D Full Cover Tempered Glass', 1.99, 1.00, 0.00, '3D Full Cover Tempered Glass For Huawei P20 Pro P10 Lite Plus Screen Protector For Huawei P20 Honor 10 Protective Glass', 'img/Products/Product 30/cover.jpg', 48.9, '45', 1.00, 'https://www.aliexpress.com/item/3D-Full-Cover-Tempered-Glass-For-Huawei-P20-Pro-P10-Lite-Plus-Screen-Protector-For-Huawei/32875999594.html?spm=2114.10010108.1000016.16.53921d4ad7lyqX&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=51038151-479f-44c4-adf2-6326177ce5c7', 0, 28, 11, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (31, 'UV Tempered Glass', 4.99, 1.00, 0.00, 'UV Tempered Glass For Samsung Note 9 8 S9 S8 S7 Edge 5D Full Liquid Glue Screen Protector For Samsung Galaxy Note 8 S8 S9 Plus', 'img/Products/Product 31/cover.jpg', 48.9, '42', 1.00, 'https://www.aliexpress.com/item/UV-Tempered-Glass-For-Samsung-Note-9-8-S9-S8-S7-Edge-5D-Full-Liquid-Glue/32888929661.html?spm=2114.search0603.3.141.7a9bda32dIbQQQ&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=06554b33-13bb-4b8c-9178-3e76571b9c13-18&algo_pvid=06554b33-13bb-4b8c-9178-3e76571b9c13&transAbTest=ae803_3', 0, 29, 11, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (32, 'Precision 45 In 1 Electron Torx Mini Magnetic', 14.00, 1.00, 0.00, 'Precision 45 In 1 Electron Torx Mini Magnetic Laptop Screwdriver Tool Set Hand Tools Kit Opening Repair Phone Laptop Tools', 'img/Products/Product 32/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Precision-45-In-1-Electron-Torx-Mini-Magnetic-Laptop-Screwdriver-Tool-Set-Hand-Tools-Kit-Opening/32957520982.html?spm=2114.search0603.6.7.7a9bda32dIbQQQ&s=p', 0, 30, 4, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (33, '2019 New Arrival Mini Power Bank', 29.88, 1.00, 0.00, '2019 New Arrival Mini Power Bank 10000mAh Small Full Display External Portable Charger PoverBank Double USB for Xiaomi Samsung', 'img/Products/Product 33/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2019-New-Arrival-Mini-Power-Bank-10000mAh-Small-Full-Display-External-Portable-Charger-PoverBank-Double-USB/33005049074.html?spm=2114.search0603.6.5.7a9bda32dIbQQQ&s=p', 0, 31, 10, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (34, 'USAMS Battery Charger Cases', 34.48, 1.00, 0.00, 'USAMS Battery Charger Cases for iPhone X XR XS 6 6s 7 8 Plus 3000/4200mAh Power Bank Case Slim External Pack Backup charger case', 'img/Products/Product 34/cover.jpg', 48.9, '20-38', 1.00, 'https://www.aliexpress.com/item/USAMS-Battery-Charger-Cases-for-iPhone-6-6s-7-Plus-2500-3800mAh-Power-Bank-Case-Ultra/32805515910.html?spm=2114.search0603.6.3.7a9bda32dIbQQQ&s=p', 0, 27, 10, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (35, 'SOODOO Solar Power Bank Waterproof', 19.90, 1.00, 0.00, 'SOODOO Solar Power Bank Waterproof 30000mAh Solar Charger 2USB Ports External Charger Powerbank for Xiaomi Iphone with LED Light', 'img/Products/Product 35/cover.jpg', 48.9, '20 - 23', 11.00, 'https://www.aliexpress.com/item/SOODOO-Solar-Power-Bank-Waterproof-30000mAh-Solar-Charger-2USB-Ports-External-Charger-Powerbank-for-Xiaomi-Iphone/33025494348.html?spm=2114.search0603.6.43.7a9bda32dIbQQQ&s=p', 0, 32, 10, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (36, 'Ultra Slim Silicone Phone', 2.68, 1.00, 0.00, 'Ultra Slim Silicone Phone Cases For iphone6 6s 7 8 X XS XR MAX Ultra Thin Shell Soft TPU Matte Phone Case for iphone5S SE 6plus', 'img/Products/Product 36/cover.jpg', 48.9, '34', 1.00, 'https://www.aliexpress.com/item/Ultra-Slim-Silicone-Phone-Cases-For-iphone6-6s-7-8-X-XS-XR-MAX-Ultra-Thin/32989023176.html?spm=2114.search0603.6.65.7a9bda32dIbQQQ&s=p', 0, 33, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (37, '300set/lot DIY 8in1 Opening Pry Set Kits Disassemble Tools', 106.26, 1.00, 0.00, '300set/lot DIY 8in1 Opening Pry Set Kits Disassemble Tools For iPhone X For Samsung Screwdriver Mobile Phone Repair Tools Kit', 'img/Products/Product 37/cover.png', 48.9, '5', 1.00, 'https://www.aliexpress.com/item/32976451560.html?spm=a2g0o.detail.1000014.33.92b15d4bF67TkO&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.125185.0&scm_id=1007.13338.125185.0&scm-url=1007.13338.125185.0&pvid=8dfbf2a8-e902-4ceb-ab86-7fbfcf296aae', 0, 34, 4, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (38, '2pcs Tempered Glass', 1.98, 11.00, 0.00, '2pcs Tempered Glass for Samsung Galaxy J2 Core J4 A6 A7 2018 A10 A20 A30 A40 A70 M10 M20 M30 Protective Film Screen Protector', 'img/Products/Product 38/cover.jpg', 48.9, '34', 1.00, 'https://www.aliexpress.com/item/2pcs-Tempered-Glass-for-Samsung-Galaxy-J2-Core-J4-A6-A7-2018-A10-A20-A30-A40/32895860919.html?spm=2114.search0603.6.95.7a9bda32dIbQQQ&s=p', 0, 35, 11, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (39, 'OnePlus 7 Pro 6 6T 5 5T Tempered Camera Glass', 2.22, 1.00, 0.00, 'for OnePlus 7 Pro 6 6T 5 5T Tempered Camera Glass Film Back Protective Lens Glass Camera Protector for One Plus 7 Pro 6 6T 5 5T', 'img/Products/Product 39/cover.jpg', 48.9, '45', 1.00, 'https://www.aliexpress.com/item/for-OnePlus-7-Pro-6-6T-5-5T-Tempered-Camera-Glass-Film-Back-Protective-Lens-Glass/33020839883.html?spm=2114.search0603.6.91.7a9bda32dIbQQQ&s=p', 0, 36, 11, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (40, '3D Tempered Glass', 3.51, 1.00, 0.00, '3D Tempered Glass For Huawei P20 Lite Full Cover 9H Protective film Explosion-proof Screen Protector For Huawei P20 Lite P20Lite', 'img/Products/Product 40/cover.jpg', 48.9, '6-32', 1.00, 'https://www.aliexpress.com/item/3D-Tempered-Glass-For-Huawei-P20-Lite-Full-Cover-9H-Protective-film-Explosion-proof-Screen-Protector/32859687094.html?spm=2114.search0603.6.105.7a9bda32dIbQQQ&s=p', 0, 37, 11, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (41, 'Marvel Avengers USB 2.0 Flash Drive', 304.45, 1.00, 0.00, 'Marvel Avengers USB 2.0 Flash Drive Pen Drive Iron Man America Captain Hammer Hulk Flash Memory Stick 8GB 16GB 32GB 64GB 128GB', 'img/Products/Product 41/cover.png', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/NEW-Avengers-Iron-Man-Hand-LED-Flash-Drive-64GB-USB-2-0-Memory-Stick-Flash-Card/32418703939.html?spm=2114.search0604.3.32.62e667c639Skv2&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=2af5c10f-1969-465e-adf7-e66d63a5f374-4&algo_pvid=2af5c10f-1969-465e-adf7-e66d63a5f374&transAbTest=ae803_3', 0, 38, 9, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (42, 'Marvel Luxury Luminous Tempered Glass Phone Case', 4.99, 1.00, 0.00, 'Marvel Luxury Luminous Tempered Glass Phone Case Iron Man Silicone Back Cover For Huawei P10 Plus P20 Pro P30 Pro Avengers Case', 'img/Products/Product 42/cover.jpg', 48.9, '33', 1.00, 'https://www.aliexpress.com/item/Marvel-Luxury-Luminous-Tempered-Glass-Phone-Case-Iron-Man-Silicone-Back-Cover-For-Huawei-P10-Plus/33015542788.html?spm=2114.search0604.8.7.39763ce5hNszGB&transAbTest=ae803_3', 0, 39, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (43, 'Marvel Luminous Glass Case ', 4.68, 1.00, 0.00, 'Venom Deadpool Black Panther Iron Man Batman Luminous Glass Case For Samsung Galaxy S8 S9 S10 e Plus Note 9 8 Marvel Phone Cover', 'img/Products/Product 43/cover.jpg', 48.9, '34', 1.00, 'https://www.aliexpress.com/item/Venom-Deadpool-Black-Panther-Iron-Man-Batman-Luminous-Glass-Case-For-Samsung-Galaxy-S8-S9-S10/32983607739.html?spm=2114.search0604.8.15.39763ce5hNszGB&transAbTest=ae803_3', 0, 40, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (44, 'Luminous Glass Phone Case', 4.69, 1.00, 0.00, 'Coque Captain Marvel Batman Iron Man Luminous Glass Phone Case For iPhone XSmax XR XS X 8 7 6s 6 Plus 5 s SE Black Panther Cover', 'img/Products/Product 44/cover.jpg', 48.9, '30 - 43', 1.00, 'https://www.aliexpress.com/item/Coque-Captain-Marvel-Batman-Iron-Man-Luminous-Glass-Phone-Case-For-iPhone-XSmax-XR-XS-X/32978896664.html?spm=2114.search0604.3.1.24da1be2IBwdvG&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=391347ea-9845-45ee-a91d-f32f3a6c2b39-0&algo_pvid=391347ea-9845-45ee-a91d-f32f3a6c2b39&transAbTest=ae803_3', 0, 41, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (45, 'Avengers  Luminous Glass Case', 4.93, 1.00, 0.00, 'Marvel Batman Superman Spiderman Luminous Glass Case For iphone 7 8 6 6s Plus X Xs Max Xr Avengers Black Panther iron Man Cover', 'img/Products/Product 45/cover.jpg', 48.9, '31', 1.00, 'https://www.aliexpress.com/item/Marvel-Batman-Superman-Spiderman-Luminous-Glass-Case-For-iphone-7-8-6-6s-Plus-X-Xs/32879600150.html?spm=2114.search0604.3.81.24da1be2IBwdvG&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=391347ea-9845-45ee-a91d-f32f3a6c2b39-10&algo_pvid=391347ea-9845-45ee-a91d-f32f3a6c2b39&transAbTest=ae803_3', 0, 42, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (46, 'Luminous Comic Marvel Phone cover', 3.99, 1.00, 0.00, 'Luminous Marvel Phone cover case For iphone X XS MAX XR 10 8 7 6 6S plus 5 5S SE cases Matte hard plastic cover Spiderman Batman', 'img/Products/Product 46/cover.jpg', 48.9, '34', 1.00, 'https://www.aliexpress.com/item/For-iphone-6-6plus-6s-plus-7-7plus-case-360-full-protection-Scrub-Matte-hard-PC/32827483734.html?spm=2114.search0604.3.17.24da1be2IBwdvG&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=391347ea-9845-45ee-a91d-f32f3a6c2b39-2&algo_pvid=391347ea-9845-45ee-a91d-f32f3a6c2b39&transAbTest=ae803_3', 0, 43, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (47, 'Marvel Captain America & Animals Luminous Glass Phone Case', 4.99, 1.00, 0.00, 'Marvel Captain America Luminous Glass Phone Case For Samsung Galaxy Note 8 9 s8 s9 s10 Plus Avengers Back Cover Fundas Coque', 'img/Products/Product 47/cover.jpg', 48.9, '33', 1.00, 'https://www.aliexpress.com/item/Marvel-Captain-America-Luminous-Glass-Phone-Case-For-Samsung-Galaxy-Note-8-9-s8-s9-s10/33011225094.html?spm=2114.search0604.3.73.24da1be2IBwdvG&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=391347ea-9845-45ee-a91d-f32f3a6c2b39-9&algo_pvid=391347ea-9845-45ee-a91d-f32f3a6c2b39&transAbTest=ae803_3', 0, 39, 2, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (48, 'Phone Holder Metal Plate Replacement Kits', 4.89, 148.42, 0.00, 'XMXCZKJ Phone Holder Metal Plate Replacement Kits With 3M Adhensive For Magnetic Cell Phone Support Car Mount Holder Accessories', 'img/Products/Product 48/cover.jpg', 48.9, '23 - 35', 1.00, 'https://www.aliexpress.com/item/Mount-Metal-Plates-Replacement-Kits-with-3M-Adhensive-for-magnetic-Car-Mount-Phone-Holder-ND-MHA001/32828078379.html?spm=2114.search0604.3.1.488c2e5fbvB5LE&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=270efe6e-97ef-4b18-a971-db96ef5a0833-0&algo_pvid=270efe6e-97ef-4b18-a971-db96ef5a0833&transAbTest=ae803_3', 0, 44, 19, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (49, 'Metal Magnetic Cell Mobile Phone Car Holder', 1.68, 1.00, 0.00, 'Metal Magnetic Cell Mobile Phone Car Holder Wall Mount Support Magnet Tablet Auto Dashboard GPS Stand for iPhone Xiaomi Huawei', 'img/Products/Product 49/cover.jpg', 48.9, '30 - 50', 1.00, 'https://www.aliexpress.com/item/Metal-Magnetic-Cell-Mobile-Phone-Car-Holder-Wall-Mount-Support-Magnet-Tablet-Auto-Dashboard-GPS-Stand/32977211298.html?spm=2114.10010108.1000016.9.2248a330Mh6aaN&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=e2aa9302-af48-4338-8da4-ceb47f83dbcf', 0, 45, 3, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (50, 'Car Holder Mini Air Vent Mount', 4.96, 1.00, 0.00, 'Car Holder Mini Air Vent Mount Cell Phone Mobile Holder Universal For iPhone X 8 7 6 PlusGPS Bracket Stand Support Accessories', 'img/Products/Product 50/cover.jpg', 48.9, '34 - 56', 1.00, 'https://www.aliexpress.com/item/Car-Holder-Mini-Air-Vent-Mount-Cell-Phone-Mobile-Holder-Universal-For-iPhone-8-7-6/32830982537.html?spm=2114.search0104.3.133.616557adnGQ4iq&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=e75d5f2d-717e-4650-b819-14b8c17dff29-17&algo_pvid=e75d5f2d-717e-4650-b819-14b8c17dff29&transAbTest=ae803_3', 0, 46, 3, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (51, 'ZLNHIV magnetic holder for phone in car', 4.27, 11.00, 0.00, 'ZLNHIV magnetic holder for phone in car round cellphone for iphone accessories mobile cell stand mount support smartphone magnet', 'img/Products/Product 51/cover.jpg', 48.9, '34 - 56', 1.00, 'https://www.aliexpress.com/item/ZLNHIV-magnetic-holder-for-phone-in-car-round-cellphone-for-iphone-accessories-mobile-cell-stand-mount/33003645853.html?spm=2114.search0104.3.169.616557adnGQ4iq&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=e75d5f2d-717e-4650-b819-14b8c17dff29-22&algo_pvid=e75d5f2d-717e-4650-b819-14b8c17dff29&transAbT', 0, 47, 3, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (52, 'JEREFISH Car Phone Holder Gps', 7.99, 1.00, 0.00, 'JEREFISH Car Phone Holder Gps Accessories Suction Cup Auto Dashboard Windshield Mobile Cell Phone Retractable Mount Stand', 'img/Products/Product 52/cover.jpg', 48.9, '23 - 35', 1.00, 'https://www.aliexpress.com/item/Car-Phone-Holder-Gps-Accessories-Suction-Cup-Soporte-Celular-Para-Auto-Dashboard-Windshield-Mobile-Cell-Retractable/32691850472.html?spm=2114.search0104.3.154.616557adnGQ4iq&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=e75d5f2d-717e-4650-b819-14b8c17dff29-20&algo_pvid=e75d5f2d-717e-4650-b819-14b8c17d', 0, 48, 3, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (53, 'XMXCZKJ Magnet Back Seat Car Mobile Cell Phone Holder', 15.89, 1.00, 0.00, 'XMXCZKJ Magnet Back Seat Car Mobile Cell Phone Holder Stand Support For iPhone Smartphone Magnetic Head Headrest Accessories', 'img/Products/Product 53/cover.jpg', 48.9, '23 - 35', 1.00, 'https://www.aliexpress.com/item/XMXCZKJ-Magnet-Back-Seat-Car-Mobile-Cell-Phone-Holder-Stand-Support-For-iPhone-Smartphone-Magnetic-Head/32949061279.html?spm=2114.search0104.3.185.616557adnGQ4iq&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=e75d5f2d-717e-4650-b819-14b8c17dff29-24&algo_pvid=e75d5f2d-717e-4650-b819-14b8c17dff29&transAb', 0, 44, 3, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (54, 'Car Automobile Mobile Cell Phone Suction Cup', 3.55, 1.00, 0.00, 'Car Automobile Mobile Cell Phone Suction Cup Stand Holder for Smartphone/Cellphone/Lenovo/LG Long Arm Hose Dual Grip Mount Clamp', 'img/Products/Product 54/cover.jpg', 48.9, '30 - 50', 1.00, 'https://www.aliexpress.com/item/Long-Arm-Neck-Car-Phone-Windshield-Window-Suction-Cup-Gooseneck-Stand-Holder-Mount-Adjustable-360-Degree/1000001400740.html?spm=2114.search0104.3.200.616557adnGQ4iq&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=e75d5f2d-717e-4650-b819-14b8c17dff29-26&algo_pvid=e75d5f2d-717e-4650-b819-14b8c17dff29&tran', 0, 36, 3, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (55, '2019 new men\'s fashion two-piece T-shirt + shorts', 24.22, 1.00, 0.00, '2019 new men\'s fashion two-piece T-shirt + shorts set men\'s summer shirt T-shirt fashion T-shirt hhirt men clothingFree shipping', 'img/Products/Product 55/cover.png', 48.9, '30 - 43', 1.00, 'pruebas', 0, 49, 20, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (56, '2019 Tide brand Sets Men Hoodies+pants Sets', 44.26, 11.00, 0.00, '2019 Tide brand Sets Men Hoodies+pants Sets Hot Sale Cotton Comfortable Brand Tracksuit Fashion Sportswear men Casual Set Pants', 'img/Products/Product 56/cover.jpg', 48.9, '30 - 43', 1.00, 'https://www.aliexpress.com/item/33026965727.html?spm=2114.search0604.3.1.2541c1d7Kf7qYJ&ws_ab_test=searchweb0_0%2Csearchweb201602_4_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103%2Csearchweb201603_52%2CppcSwitch_0&algo_expid=6db3a69e-7777-4d3a-b37c-11837eaa1b57-0&algo_pvid=6db3a69e-7777-4d3a-b37c-11837eaa1b57', 0, 50, 20, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (57, 'ICEbear 2018 new winter men\'s down jacket', 377.00, 1.00, 0.00, 'ICEbear 2018 new winter men\'s down jacket high quality detachable hat male\'s jackets thick warm fur collar clothing MWY18963D', 'img/Products/Product 57/cover.jpg', 48.9, '30 - 43', 1.00, 'https://www.aliexpress.com/item/ICEbear-2018-new-winter-men-s-down-jacket-high-quality-detachable-hat-male-s-jackets-thick/32904905150.html?af=607243&aff_platform=aaf&cpt=1561180966989&afref=&dp=4fca2c4ee2594d247c91cebda30de2b9&onelink_item_from=32904905150&onelink_thrd=0.015&onelink_page_from=ITEM_DETAIL&cv=47843&onelink_item_to=32904905150&pvid=7f41b6f5-ea71-4d89-b43c-d32a21cd1764&mall_affr=pr3&onelink_duration=0.999602&sk=VnYZvQVf&aff_trace_key=3d2f3c63c5e4439e8ae84df1d8dc9252-1561180966989-0', 0, 51, 13, '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (58, '2019 Summer Polo Shirt Men', 10.39, 1.00, 0.00, '2019 Summer Polo Shirt Men Short Sleeve Casual Slim Fit Cotton Polo Shirt', 'img/Products/Product 58/cover.jpg', 48.9, '30 - 43', 11.00, 'https://www.aliexpress.com/item/2019-Summer-Polo-Shirt-Men-Short-Sleeve-Casual-Slim-Fit-Cotton-Polo-Shirt/1000008720754.html?spm=2114.search0603.3.51.4bbc1a97mf9MM4&s=p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-6&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 52, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (59, 'KUEGOU New Summer Mens', 42.65, 1.00, 0.00, 'KUEGOU New Summer Mens Knitted Poloshirts 100% Cotton Striped Blue Brand Clothing Man\'s Wear Short Sleeve Slim Clothes 16972', 'img/Products/Product 59/cover.jpg', 48.9, '30 - 43', 1.00, 'https://www.aliexpress.com/item/2017-Summer-Mens-Fashion-Polo-Shirts-100-Cotton-Striped-Blue-Color-Brand-Clothing-Man-s-Wear/32790872038.html?spm=2114.search0603.3.57.4bbc1a97mf9MM4&s=p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-7&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 53, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (60, 'Casual Jogger Brand Men', 14.32, 1.00, 0.00, 'Casual Jogger Brand Men Pants Hip Hop Harem Joggers Pants 2019 Male Trousers Mens Joggers Solid Pants Sweatpants Large Size XXL', 'img/Products/Product 60/cover.jpg', 48.9, '30 - 43', 1.00, 'https://www.aliexpress.com/item/Casual-Jogger-Brand-Men-Pants-Hip-Hop-Harem-Joggers-Pants-2019-Male-Trousers-Mens-Joggers-Solid/1000007591730.html?spm=2114.search0603.3.65.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-8&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 54, 14, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (61, '2019 Brand t shirt Men Sets Fashion Summer', 17.44, 1.00, 0.00, '2019 Brand t shirt Men Sets Fashion Summer cotton short sleeve Sporting Suit T-shirt +shorts Mens 2 Pieces Sets casual clothing', 'img/Products/Product 61/cover.png', 48.9, '30 - 43', 1.00, 'https://www.aliexpress.com/item/32996148836.html?spm=2114.search0603.3.64.c2a02b0dNHP6SS&ws_ab_test=searchweb0_0%2Csearchweb201602_4_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103%2Csearchweb201603_52%2CppcSwitch_0&algo_expid=0efbd0a5-e50d-40fb-b2cd-f8e0ea5b0f30-8&algo_pvid=0efbd0a5-e50d-40fb-b2cd-f8e0ea5b0f30', 0, 55, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (62, '2018 Mens Military Army T Shirt', 13.30, 11.00, 0.00, '2018 Mens Military Army T Shirt 2017 Men Star Loose Cotton T-shirt O-neck Alpha America Size Short Sleeve Tshirts', 'img/Products/Product 62/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2018-Mens-Military-Army-T-Shirt-2017-Men-Star-Loose-Cotton-T-shirt-O-neck-Alpha/32952989906.html?spm=2114.search0603.3.81.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-10&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 56, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (63, '2019 New Men Joggers Brand Male', 15.37, 1.00, 0.00, '2019 New Men Joggers Brand Male Trousers Casual Pants Sweatpants Jogger Grey Casual Elastic Cotton GYMS Fitness Workout Dar XXXL', 'img/Products/Product 63/cover.jpg', 48.9, '30 - 43', 1.00, 'https://www.aliexpress.com/item/2019-New-Men-Joggers-Brand-Male-Trousers-Casual-Pants-Sweatpants-Jogger-Grey-Casual-Elastic-Cotton-GYMS/33011637483.html?spm=2114.search0603.3.98.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-12&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae', 0, 57, 14, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (64, 'Liseaven T-Shirt Men Cotton T Shirt', 23.98, 1.00, 0.00, 'Liseaven T-Shirt Men Cotton T Shirt Full Sleeve tshirt Men Solid Color T-shirts tops&tees Mandarin Collar Long Shirt', 'img/Products/Product 64/cover.jpg', 48.9, '30 - 43', 1.00, 'https://www.aliexpress.com/item/Liseaven-T-Shirt-Men-Cotton-T-Shirt-Full-Sleeve-tshirt-Men-Solid-Color-T-shirts-tops/32869576765.html?spm=2114.search0603.3.147.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-18&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 58, 21, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (65, 'Rick and Morty By Jm2 Art 3D t shirt Men', 11.56, 1.00, 0.00, 'Rick and Morty By Jm2 Art 3D t shirt Men tshirt Summer Anime T-Shirt Short Sleeve Tees O-neck Tops Drop Ship', 'img/Products/Product 65/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Rick-and-Morty-By-Jm2-Art-3D-t-shirt-Men-tshirt-Summer-Anime-T-Shirt-Short/32909616206.html?spm=2114.search0603.3.172.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-21&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 59, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (66, '2019 Summer Fashion Men\'s T Shirt Casual ', 17.98, 1.00, 0.00, '2019 Summer Fashion Men\'s T Shirt Casual Patchwork Short Sleeve T Shirt Mens Clothing Trend Casual Slim Fit Hip-Hop Top Tees 5XL', 'img/Products/Product 66/cover.jpg', 48.9, '25', 11.00, 'https://www.aliexpress.com/item/2016-Summer-Fashion-Brand-Mens-Solid-Color-T-Shirt-Casual-Short-Sleeve-Heap-Turtleneck-Open-Front/32688646069.html?spm=2114.search0603.3.163.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-20&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 60, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (67, 'TShirts Men\'s Summer Slim Fit Casual Pattern', 14.37, 1.00, 0.00, 'TShirts Men\'s Summer Slim Fit Casual Pattern Large Size Short Sleeve Hoodie Top Blouse Casual Men Fashion High Quality c0509', 'img/Products/Product 67/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/TShirts-Men-s-Summer-Slim-Fit-Casual-Pattern-Large-Size-Short-Sleeve-Hoodie-Top-Blouse-Casual/33014666507.html?spm=2114.search0603.3.188.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-23&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 61, 22, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (68, '2019 new multi-color T-shirt men\'s women\'s', 9.82, 11.00, 0.00, '2019 new multi-color T-shirt men\'s women\'s T-shirt summer skateboard T-shirt boy skateboard t shirt top', 'img/Products/Product 68/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2019-new-multi-color-T-shirt-men-s-women-s-T-shirt-summer-skateboard-T-shirt/32974459513.html?spm=2114.search0603.3.196.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-24&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 62, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (69, '2019 New Summer T Shirts Mens', 9.98, 1.00, 0.00, '2019 New Summer T Shirts Mens Tshirts Male Fitness T-Shirts Slim Print black Short sleeve women Polyester Boys Tops Tees', 'img/Products/Product 69/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2019-New-Summer-T-Shirts-Mens-Tshirts-Male-Fitness-T-Shirts-Slim-Print-black-Short-sleeve/33005482793.html?spm=2114.10010108.1000016.44.4f43275688teKl&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=e7a44425-0031-4a5c-94e3-cf81771b4286', 0, 63, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (70, 'Autumn Men Pants Hip Hop Harem Joggers', 20.90, 1.00, 0.00, 'Autumn Men Pants Hip Hop Harem Joggers Pants 2019 New Male Trousers Mens Joggers Solid Multi-pocket Pants Sweatpants M-4XL', 'img/Products/Product 70/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Autumn-Men-Pants-Hip-Hop-Harem-Joggers-Pants-2019-New-Male-Trousers-Mens-Joggers-Solid-Multi/32926498928.html?spm=2114.search0603.3.220.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-27&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 64, 14, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (71, 'TUNSECHY NEW Marvel Super Heroes T shirt Men ', 7.99, 1.00, 0.00, 'TUNSECHY NEW Marvel Super Heroes Avenger Batman T shirt Men Compression Base Layer Long Sleeve Thermal Under Top Fitness', 'img/Products/Product 71/cover.jpg', 48.9, '25', 11.00, 'https://www.aliexpress.com/item/TUNSECHY-NEW-Marvel-Super-Heroes-Avenger-Batman-T-shirt-Men-Compression-Base-Layer-Long-Sleeve-Thermal/32921323796.html?spm=2114.search0603.3.309.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-38&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae', 0, 65, 23, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (72, 'Marvel The Avengers 4 Endgame Quantum Realm Cosplay', 18.00, 1.00, 0.00, 'Marvel The Avengers 4 Endgame Quantum Realm Cosplay Costume Hoodies Men Hooded Avengers Zipper End Game Sweatshirt Jacket', 'img/Products/Product 72/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Marvel-The-Avengers-4-Endgame-Quantum-Realm-Cosplay-Costume-Hoodies-Men-Hooded-Avengers-Zipper-End-Game/32993004062.html?spm=2114.10010108.1000010.1.34056524DE4tLe&gps-id=pcDetailLeftTrendProduct&scm=1007.13438.125069.0&scm_id=1007.13438.125069.0&scm-url=1007.13438.125069.0&pvid=08f2d4a5-4568-46f3-afd4-c997ebaa6e90', 0, 66, 13, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (73, 'Heroes 3D Printed T-shirts', 7.99, 1.00, 0.00, 'Black Panther 3D Printed T-shirts Captain America Civil War Tee Long Sleeve Cosplay Halloween Costumes Compression Tops Male For', 'img/Products/Product 73/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Black-Panther-3D-Printed-T-shirts-Captain-America-Civil-War-Tee-Long-Sleeve-Cosplay-Halloween-Costumes/32921399545.html?spm=2114.10010108.1000013.2.2ea0b476LJVwKO&gps-id=pcDetailBottomMoreThisSeller&scm=1007.13339.99734.0&scm_id=1007.13339.99734.0&scm-url=1007.13339.99734.0&pvid=54bbc554-47ee-4cd1-9213-2e970194f61f', 0, 65, 23, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (74, 'TUNSECHY brand Newest long sleeve compression shirt men\'s', 10.88, 1.00, 0.00, 'TUNSECHY brand Newest long sleeve compression shirt men\'s marvel captain america/Ironman/Sipder Man/superman Tights t shirt', 'img/Products/Product 74/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/TUNSECHY-brand-Newest-long-sleeve-compression-shirt-men-s-marvel-captain-america-Ironman-Sipder-Man-superman/32919390605.html?spm=2114.10010108.1000013.3.2ea0b476LJVwKO&gps-id=pcDetailBottomMoreThisSeller&scm=1007.13339.99734.0&scm_id=1007.13339.99734.0&scm-url=1007.13339.99734.0&pvid=54bbc554-47ee-4cd1-9213-2e970194f61f', 0, 65, 23, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (75, 'Funny birthday gifts for huaband boyfriend men', 11.75, 11.00, 0.00, 'Funny birthday gifts for huaband boyfriend men it works on my machine short sleeve cotton computer programmer t shirt t-shirt', 'img/Products/Product 75/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Funny-birthday-gifts-for-huaband-boyfriend-men-it-works-on-my-machine-short-sleeve-cotton-computer/32881951633.html?spm=2114.search0603.3.351.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-43&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_', 0, 67, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (76, '2017 summer Hot selling Men V neck t shirt', 10.25, 1.00, 0.00, '2017 summer Hot selling Men V neck t shirt cotton short sleeve tops high quality Casual Men Slim Fit Classic Brand t shirts', 'img/Products/Product 76/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2017-summer-Hot-selling-Men-V-neck-t-shirt-cotton-short-sleeve-tops-high-quality-Casual/32820314336.html?spm=2114.search0603.3.342.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-42&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 68, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (77, 'Sinicism Store Men Harajuku Cotton', 20.63, 1.00, 0.00, 'Sinicism Store Men Harajuku Cotton Linen Tshirt 2019 Mens Summer Solid Streetwear Fashions White Tshirts Male Summer T-shirts', 'img/Products/Product 77/cover.jpg', 48.9, '23 - 35', 1.00, 'https://www.aliexpress.com/item/Sinicism-Store-Men-Harajuku-Cotton-Linen-Tshirt-2019-Mens-Summer-Solid-Streetwear-Fashions-White-Tshirts-Male/33001819388.html?spm=2114.search0603.3.367.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-45&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAb', 0, 69, 24, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (78, 'Spring Beautiful Flowers Print Casual T-shirt Men/Women', 7.34, 1.00, 0.00, '2019 New Spring Beautiful Flowers Print Casual T-shirt Men/Women Summer Tees Quick Dry 3D Print Tshirts Tops Fashion Tops', 'img/Products/Product 78/cover.jpg', 48.9, '26', 1.00, 'https://www.aliexpress.com/item/32982897592.html?spm=2114.search0604.3.133.b43c308ddYI1kA&ws_ab_test=searchweb0_0%2Csearchweb201602_4_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10881_10059_10884_10887_321_322_10103%2Csearchweb201603_52%2CppcSwitch_0&algo_expid=d296966b-d5bf-4b1d-8e25-1e18183cb87d-17&algo_pvid=d296966b-d5bf-4b1d-8e25-1e18183cb87d', 0, 70, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (79, 'Men Casual T-Shirt Curve Hem Side', 18.90, 1.00, 0.00, 'Men Casual T-Shirt Curve Hem Side With Zipper Short Sleeve Streetwear Men Long line Hip Pop Style Tops Fashion Men Extend Swag T', 'img/Products/Product 79/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Men-Casual-T-Shirt-Curve-Hem-Side-With-Zipper-Short-Sleeve-Streetwear-Men-Long-line-Hip/32854985977.html?spm=2114.search0603.3.359.4bbc1a97mf9MM4&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b-44&algo_pvid=c2a25b60-61a7-4cda-bc02-0f9a66f1878b&transAbTest=ae803_3', 0, 71, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (80, 'Hot new sports suit T-shirt + pants two-piece men\'s', 30.59, 1.00, 0.00, 'Hot new sports suit T-shirt + pants two-piece men\'s hip hop summer cotton sportswear men\'s fitness training fitness clothingsuit', 'img/Products/Product 80/cover.jpg', 48.9, '25', 11.00, 'https://www.aliexpress.com/item/32959296414.html?spm=2114.search0603.3.192.11e61a97NrjpqZ&ws_ab_test=searchweb0_0%2Csearchweb201602_4_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103%2Csearchweb201603_52%2CppcSwitch_0&algo_expid=5393e0e0-a16b-48b8-9e8a-0c789864f801-23&algo_pvid=5393e0e0-a16b-48b8-9e8a-0c789864f801', 0, 72, 23, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (81, 'Zagaa New 2019 Polo Men\'s Shirt', 8.21, 1.00, 0.00, 'Zagaa New 2019 Polo Men\'s Shirt Cotton Short Sleeve Shirt Casual Shirts Summer Breathable Solid Male Polo Shirt Plus Size S-3XL', 'img/Products/Product 81/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Zagaa-New-2019-Polo-Men-s-Shirt-Cotton-Short-Sleeve-Shirt-Casual-Shirts-Summer-Breathable-Solid/1000007562297.html?spm=2114.search0603.3.35.7bb37957ytBJUC&s=p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-5&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803', 0, 73, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (82, 'Man\'s T Shirt New Hot Seven Dragon Ball', 11.68, 1.00, 0.00, 'Man\'s T Shirt New Hot Seven Dragon Ball Series 3D Digital Printing Men\'s Short Sleeve T Shirt Game Of Thrones T Shirt Top Tees', 'img/Products/Product 82/cover.jpg', 48.9, '25', 11.00, 'https://www.aliexpress.com/item/Man-s-T-Shirt-New-Hot-Seven-Dragon-Ball-Series-3D-Digital-Printing-Men-s-Short/33022796142.html?spm=2114.search0603.3.59.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-8&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 74, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (83, '2019 New Fashion Brand Men\'s Short', 17.95, 1.00, 0.00, '2019 New Fashion Brand Men\'s Short Sleeve T Shirt Spring O Neck Patchwork T Shirt Men Hot Sale Plus Size Top Tees Shirts M-5XL', 'img/Products/Product 83/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2019-New-Fashion-Brand-Men-s-Short-Sleeve-T-Shirt-Spring-O-Neck-Patchwork-T-Shirt/32848327971.html?spm=2114.search0603.3.118.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-15&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 60, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (84, 'Gradient color Fashion T Shirt Men', 13.33, 11.00, 0.00, 'Gradient color Fashion T Shirt Men Fast compression Breathable Mens Short Sleeve Fitness Mens t-shirt Gyms Tee Tight Casual Top', 'img/Products/Product 84/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Gradient-color-Fashion-T-Shirt-Men-Fast-compression-Breathable-Mens-Short-Sleeve-Fitness-Mens-t-shirt/32982102712.html?spm=2114.search0603.3.109.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-14&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae8', 0, 75, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (85, 'haha joker 3D Print Sweatshirt Hoodies Men', 17.16, 1.00, 0.00, 'haha joker 3D Print Sweatshirt Hoodies Men and women Hip Hop Funny Autumn Streetwear Hoodies Sweatshirt For Couples Clothes', 'img/Products/Product 85/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/haha-joker-3D-Print-Sweatshirt-Hoodies-Men-and-women-Hip-Hop-Funny-Autumn-Streetwear-Hoodies-Sweatshirt/32955149073.html?spm=2114.search0603.3.144.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-18&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=a', 0, 76, 25, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (86, 'Mayma Rick And Morty T shirt', 13.06, 1.00, 0.00, 'Mayma Rick And Morty T shirt Cool Anime Casual The Science Face Tshirt Geek 100% Cotton Tops Tee EU Size', 'img/Products/Product 86/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Mayma-Rick-And-Morty-T-shirt-Cool-Anime-Casual-The-Science-Face-Tshirt-Geek-100-Cotton/32867852316.html?spm=2114.search0604.3.1.5ee87238kDcxB7&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10130_10068_10547_319_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10881_10059_10884_10887_321_322_10103,searchweb201603_55,ppcSwitch_0&algo_expid=28bfcf8a-3bd9-491c-b595-d281af0d2a0d-0&algo_pvid=28bfcf8a-3bd9-491c-b595-d281af0d2a0d&transAbTest=ae803_3', 0, 77, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (87, '2019 Summer flags T-shirt  men\'s', 9.82, 1.00, 0.00, '2019 Summer Russian flag men\'s casual fashion T-shirt round neck cool and lightweight man\'s T-shirt', 'img/Products/Product 87/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2019-Summer-Russian-flag-men-s-casual-fashion-T-shirt-round-neck-cool-and-lightweight-man/32974523420.html?spm=2114.search0603.3.127.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-16&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 62, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (88, 'YEMEKE New Men Short Sleeve', 15.98, 1.00, 0.00, 'YEMEKE New Men Short Sleeve Cotton t-shirt Summer Casual Fashion Gyms Fitness Bodybuilding T shirt Male Slim Tees Tops Clothing', 'img/Products/Product 88/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/YEMEKE-New-Men-Short-Sleeve-Cotton-t-shirt-Summer-Casual-Fashion-Gyms-Fitness-Bodybuilding-T-shirt/32891063385.html?spm=2114.search0603.3.160.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-20&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_', 0, 78, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (89, 'ANIME One Punch Man Printed men T shirt ', 6.63, 1.00, 0.00, 'THE COOLMIND 100% cotton ANIME One Punch Man Printed men T shirt Fashion cool confortable men\'s Tshirt casual t-shirt for men', 'img/Products/Product 89/cover.jpg', 48.9, '25', 11.00, 'https://www.aliexpress.com/item/COOLMIND-Brand-Men-s-Anime-One-Punch-Man-Print-T-shirt-One-Punch-Man-Manga-Top/32793224237.html?spm=2114.search0603.3.201.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-25&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 79, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (90, 'Men\'s T Shirt 2019 Summer', 17.98, 1.00, 0.00, 'Men\'s T Shirt 2019 Summer Slim Fitness Hooded Short-Sleeved Tees Male Camisa Masculina Sportswer T-Shirt Slim Tshirt Homme 5XL', 'img/Products/Product 90/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Men-s-T-Shirt-2017-Summer-Slim-Fitness-Hooded-Short-Sleeved-Tees-Male-Camisa-Masculina-Sportswer/32800215055.html?spm=2114.search0603.3.210.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-26&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 80, 22, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (91, '2019 New design t shirt men/women marvel Avengers', 11.90, 1.00, 0.00, '2019 New design t shirt men/women marvel Avengers Endgame 3D print t-shirts MAN Short sleeve Harajuku style tshirt tops US SIZE', 'img/Products/Product 91/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2019-New-design-t-shirt-men-women-marvel-Avengers-Endgame-3D-print-t-shirts-MAN-Short/1000008689865.html?spm=2114.search0603.3.236.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-29&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 81, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (92, 'Brand Mens muscle T shirt bodybuilding', 12.99, 1.00, 0.00, 'Brand Mens muscle T shirt bodybuilding fitness men tops cotton singlets Plus Big size TShirt Cotton Mesh Short Sleeve Tshirt', 'img/Products/Product 92/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Brand-Mens-muslce-T-shirt-bodybuilding-fitness-men-tops-cotton-singlets-Plus-Big-size-T-Shirt/32758406096.html?spm=2114.search0603.3.227.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-28&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 82, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (93, '2019 Men\'s T Shirt Pocket Tees', 14.95, 1.00, 0.00, '2019 Men\'s T Shirt Pocket Tees The Lion King Scar Betrays Mufasa Long Live the King Childhood Trauma', 'img/Products/Product 93/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2017-Novel-nice-print-design-Preci-pocket-summer-T-shirt-Cool-men-spring-summer-shirt-brand/32810877195.html?spm=2114.search0603.3.286.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-35&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 83, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (94, 'Stickers Lion King Remember Who You Are', 1.89, 1.00, 0.00, 'Lion King Remember Who You Are 2pcs Deer Laptop Car Wall Vinyl Decal Sticker Art Decor Switch Sticker Refrigerator Sticker C124', 'img/Products/Product 94/cover.jpg', 48.9, '34', 1.00, 'https://www.aliexpress.com/item/Lion-King-Remember-Who-You-Are-2pcs-Deer-Laptop-Car-Wall-Vinyl-Decal-Sticker-Art-Decor/32887218408.html?spm=2114.10010108.1000016.13.10932562qEU3J0&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.13338.112281.000000000000000&scm_id=1007.13338.112281.000000000000000&scm-url=1007.13338.112281.000000000000000&pvid=7049ffc7-f5df-47bc-a10e-6cff9525beab', 0, 84, 26, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (95, 'Hot Selling Summer Short Sleeve Men T Shirt', 18.16, 11.00, 0.00, 'Hot Selling Summer Short Sleeve Men T Shirt Cotton Blended Solid Mens T-shirt Casual Slim Tee Shirt Homme', 'img/Products/Product 95/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Hot-Selling-Summer-Short-Sleeve-Men-T-Shirt-Cotton-Blended-Solid-Mens-T-shirt-Casual-Slim/32970952891.html?spm=2114.search0603.3.293.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-36&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 64, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (96, 'SWENEARO Men T-Shirts Plus', 9.98, 1.00, 0.00, 'SWENEARO Men T-Shirts Plus Size 5XL 4XL Tee Shirt Homme Summer Short Sleeve Men\'s T Shirts Male TShirts Camiseta Tshirt Homme', 'img/Products/Product 96/cover.jpg', 48.9, '25', 11.00, 'https://www.aliexpress.com/item/SWENEARO-Men-T-Shirts-Plus-Size-5XL-4XL-Tee-Shirt-Homme-Summer-Short-Sleeve-Men-s/32830368613.html?spm=2114.search0603.3.302.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-37&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 85, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (97, 'Men T Shirt 2019 Summer Fashion Star Wars', 8.60, 1.00, 0.00, 'Men T Shirt 2019 Summer Fashion Star Wars Yoda/Darth Vader Streetwear T-Shirt Men\'s Casual T Shirts Masks Words Hip Hop Tops Tee', 'img/Products/Product 97/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Mens-2016-Summer-Fashion-Streetwear-T-Shirt-Man-Casual-Short-Sleeve-O-Neck-T-Shirts-Gas/32636325131.html?spm=2114.search0603.3.319.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-39&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 86, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (98, 'Sinicism 5XL Men\'s Wild Crotch Harem Pants', 30.13, 1.00, 0.00, 'Sinicism 5XL Men\'s Wild Crotch Harem Pants Summer Baggy Pure Cotton Trousers Plus Size Male Wild-leg Loose Pants Drawstring', 'img/Products/Product 98/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Sinicism-5XL-Men-s-Wild-Crotch-Harem-Pants-Summer-Baggy-Pure-Cotton-Trousers-Plus-Size-Male/32870397243.html?spm=2114.search0603.3.345.7bb37957ytBJUC&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=d8feb038-1c20-4046-887e-23a98e29dc97-42&algo_pvid=d8feb038-1c20-4046-887e-23a98e29dc97&transAbTest=ae803_3', 0, 69, 27, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (99, 'Vomint 2019 New Men\'s Pants Straight', 45.98, 1.00, 0.00, 'Vomint 2019 New Men\'s Pants Straight Loose Casual Trousers Large Size Cotton Fashion Men\'s Business Suit Pants Green Brown Grey', 'img/Products/Product 99/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Vomint-2019-New-Men-s-Pants-Straight-Loose-Casual-Trousers-Large-Size-Cotton-Fashion-Men-s/32908993591.html?spm=2114.search0603.3.76.6b705913V1hrz2&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=4b70da5a-c6f3-4b32-9750-61388c6a8c54-10&algo_pvid=4b70da5a-c6f3-4b32-9750-61388c6a8c54&transAbTest=ae803_3', 0, 87, 28, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (100, 'SHANBAO brand original Chinese style', 21.42, 1.00, 0.00, 'SHANBAO brand original Chinese style embroidery cotton fashion loose harem pants 2019 summer men\'s straight wide leg pants K1617', 'img/Products/Product 100/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/SHANBAO-brand-original-Chinese-style-embroidery-cotton-fashion-loose-harem-pants-2019-summer-men-s-straight/32962269217.html?spm=2114.search0603.3.275.6b705913V1hrz2&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=4b70da5a-c6f3-4b32-9750-61388c6a8c54-34&algo_pvid=4b70da5a-c6f3-4b32-9750-61388c6a8c54&transAbTe', 0, 88, 27, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (101, 'Sinicism Store Mens 2019 New Beach Pants Male', 35.16, 1.00, 0.00, 'Sinicism Store Mens 2019 New Beach Pants Male Summer Casual Calf-Length Pants Man Carp Embroidery Baggy Loose Trousers', 'img/Products/Product 101/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Sinicism-Store-Mens-2019-New-Beach-Pants-Male-Summer-Casual-Calf-Length-Pants-Man-Carp-Embroidery/32879596001.html?spm=2114.search0603.3.326.6b705913V1hrz2&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=4b70da5a-c6f3-4b32-9750-61388c6a8c54-40&algo_pvid=4b70da5a-c6f3-4b32-9750-61388c6a8c54&transAbTest=ae803_3', 0, 69, 27, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (102, 'Fall 2019 hot style muscle men\'s sportswear', 24.66, 1.00, 0.00, 'Fall 2019 hot style muscle men\'s sportswear casual hoodie patchwork hooded suit jogger hip hop men\'s fashion men\'s wear', 'img/Products/Product 102/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Fall-2019-hot-style-muscle-men-s-sportswear-casual-hoodie-patchwork-hooded-suit-jogger-hip-hop/33006130370.html?spm=2114.search0603.3.44.19872410XRASnj&s=p&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=01084567-d5a9-4bb6-9209-495053bb9cc8-6&algo_pvid=01084567-d5a9-4bb6-9209-495053bb9cc8&transAbTest=ae803_3', 0, 89, 20, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (103, 'Fashion Men Business Casual Pants', 17.51, 1.00, 0.00, 'Fashion Men Business Casual Pants Cotton Slim Straight Trousers Spring Summer Long Pants -MX8', 'img/Products/Product 103/cover.jpg', 48.9, '38', 1.00, 'https://www.aliexpress.com/item/Fashion-Men-Business-Casual-Pants-Cotton-Slim-Straight-Trousers-Spring-Summer-Long-Pants-MX8/32819202501.html?spm=2114.search0603.3.180.19872410XRASnj&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=01084567-d5a9-4bb6-9209-495053bb9cc8-23&algo_pvid=01084567-d5a9-4bb6-9209-495053bb9cc8&transAbTest=ae803_3', 0, 90, 28, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (104, 'YEMEKE Brand T-Shirts 2019 Summer Short', 15.98, 1.00, 0.00, 'YEMEKE Brand T-Shirts 2019 Summer Short Sleeve O-neck Stripe Printed Loose Slim T shirt Mens Tops Tee', 'img/Products/Product 104/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/YEMEKE-Brand-T-Shirts-2019-Summer-Short-Sleeve-O-neck-Stripe-Printed-Loose-Slim-T-shirt/32876708399.html?spm=2114.search0603.3.196.19872410XRASnj&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=01084567-d5a9-4bb6-9209-495053bb9cc8-25&algo_pvid=01084567-d5a9-4bb6-9209-495053bb9cc8&transAbTest=ae803_3', 0, 78, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (105, '2019 New Rammstein T Shirt', 9.45, 11.00, 0.00, '2019 New Rammstein T Shirt Casual Harajuku Streetwear T-shirts Men Short Sleeve Summer Top Tee Hipster Rock Tshirt', 'img/Products/Product 105/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2019-New-Rammstein-T-Shirt-Casual-Harajuku-Streetwear-T-shirts-Men-Short-Sleeve-Summer-Top-Tee/33025021199.html?spm=2114.search0603.3.320.19872410XRASnj&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=01084567-d5a9-4bb6-9209-495053bb9cc8-40&algo_pvid=01084567-d5a9-4bb6-9209-495053bb9cc8&transAbTest=ae803_3', 0, 91, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (106, 'Vomint Brand New Mens Casual Pant', 35.98, 1.00, 0.00, 'Vomint Brand New Mens Casual Pant High Stretch Elastic Fabric Skinny Slim Cutting Trouser Pocket Badge Plus Size 44 46 V7S1P008', 'img/Products/Product 106/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2017-Brand-New-Mens-Casual-Pant-High-Stretch-Elastic-Fabric-Skinny-Slim-Cutting-Trouser-Pocket-Badge/32810077797.html?spm=2114.search0603.3.345.19872410XRASnj&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=01084567-d5a9-4bb6-9209-495053bb9cc8-43&algo_pvid=01084567-d5a9-4bb6-9209-495053bb9cc8&transAbTest=ae80', 0, 87, 28, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (107, '2019 New Military Cargo Pants Men', 38.16, 1.00, 0.00, '2019 New Military Cargo Pants Men Camouflage Tactical Casual Cotton Casual Trousers Men Pantalon Hombre', 'img/Products/Product 107/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/2019-New-Military-Cargo-Pants-Men-Camouflage-Tactical-Casual-Cotton-Casual-Trousers-Men-Pantalon-Hombre/32861071297.html?spm=2114.search0603.3.111.c2a02b0dobqPt9&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=42fb2e6e-e1b6-48b1-a3e5-e8f726358af4-14&algo_pvid=42fb2e6e-e1b6-48b1-a3e5-e8f726358af4&transAbTest=a', 0, 64, 28, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (108, 'BIANYILONG 2019 t shirt men', 11.50, 1.00, 0.00, 'BIANYILONG 2019 t shirt men Newest Venom Marvel t-shirt 3D Printed T-shirts Men Women Casual Shirt Fitness T Shirt Tees Tops', 'img/Products/Product 108/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/BIANYILONG-2019-t-shirt-men-Newest-Venom-Marvel-t-shirt-3D-Printed-T-shirts-Men-Women/32972594727.html?spm=2114.search0603.3.120.c2a02b0dobqPt9&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=42fb2e6e-e1b6-48b1-a3e5-e8f726358af4-15&algo_pvid=42fb2e6e-e1b6-48b1-a3e5-e8f726358af4&transAbTest=ae803_3', 0, 92, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (109, 'Cotton Linen Joggers Men\'s', 14.80, 1.00, 0.00, 'Cotton Linen Joggers Black Men\'s Harem Pants Harajuku Fitness Casual Ankle-Length Mens Trousers Summer Streetwear Clothes Male', 'img/Products/Product 109/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Cotton-Linen-Joggers-Black-Men-s-Harem-Pants-Harajuku-Fitness-Casual-Ankle-Length-Mens-Trousers-Summer/33011896374.html?spm=2114.search0603.3.153.c2a02b0dobqPt9&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=42fb2e6e-e1b6-48b1-a3e5-e8f726358af4-19&algo_pvid=42fb2e6e-e1b6-48b1-a3e5-e8f726358af4&transAbTest=ae', 0, 93, 27, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (110, '2019 Elastic Mens T-Shirt V-Neck Long', 11.98, 1.00, 0.00, '2019 Elastic Mens T-Shirt V-Neck Long Sleeve Men T Shirt For Male Big Size Lycra And Cotton TShirt Business Man Tees', 'img/Products/Product 110/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/Lycra-cotton-men-s-long-sleeve-v-neck-t-shirt-men-v-neck-long-sleeve-T/32825287873.html?spm=2114.search0604.3.1.27eb47c0HAkQm9&ws_ab_test=searchweb0_0,searchweb201602_7_10065_10068_10130_10547_319_10059_10884_317_10548_10545_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_55,ppcSwitch_0&algo_expid=fa3b01f0-eceb-4adb-a691-47a13f954f8e-0&algo_pvid=fa3b01f0-eceb-4adb-a691-47a13f954f8e&transAbTest=ae803_3', 0, 94, 21, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` VALUES (111, '100% cotton digging the moon print casual mens o-neck t shirts', 7.35, 1.00, 0.00, '100% cotton digging the moon print casual mens o-neck t shirts fashion men\'s tops men T-shirt short sleeve men tshirt 2017', 'img/Products/Product 111/cover.jpg', 48.9, '25', 1.00, 'https://www.aliexpress.com/item/32793121678.html?spm=2114.search0604.3.175.4802512f7gNkzT&ws_ab_test=searchweb0_0%2Csearchweb201602_4_10065_10130_10068_10547_319_10546_317_10548_10545_10696_10084_453_454_10083_10618_10307_537_536_10059_10884_10887_321_322_10103%2Csearchweb201603_52%2CppcSwitch_0&algo_expid=f6e72841-8e12-47c5-ab1b-d16e04c9f119-22&algo_pvid=f6e72841-8e12-47c5-ab1b-d16e04c9f119', 0, 79, 12, '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);

-- ----------------------------
-- Table structure for productvariants
-- ----------------------------
DROP TABLE IF EXISTS `productvariants`;
CREATE TABLE `productvariants`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `variant` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `value` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `price` decimal(5, 2) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `productvariants_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of productvariants
-- ----------------------------
INSERT INTO `productvariants` VALUES (1, 'Phone', 'iPhone 6', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (2, 'Phone', 'iPhone 6S', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (3, 'Phone', 'iPhone 6 Plus', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (4, 'Phone', 'iPhone 6S Plus', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (5, 'Phone', 'iPhone 7', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (6, 'Phone', 'iPhone 7 Plus', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (7, 'Phone', 'iPhone 8', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (8, 'Phone', 'iPhone 8 Plus', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (9, 'Phone', 'iPhone X', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (10, 'Phone', 'iPhone XR', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (11, 'Phone', 'iPhone XS', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (12, 'Phone', 'iPhone XS MAX', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (13, 'Phone', 'iPhone 3G', 2.55, 1, '2020-03-07 00:28:31', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (14, 'Color', 'Silver', 2.55, 1, '2020-03-07 00:32:20', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (15, 'Color', 'Red', 2.55, 1, '2020-03-07 00:32:20', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (16, 'Color', 'Gold', 2.55, 1, '2020-03-07 00:32:20', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (17, 'Color', 'Black', 2.55, 1, '2020-03-07 00:32:20', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (18, 'Phone', 'Samsung S8', 3.99, 3, '2020-03-10 01:11:22', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (19, 'Phone', 'Samsung S8 Plus', 3.99, 3, '2020-03-10 01:11:22', 1, NULL, NULL, 0);
INSERT INTO `productvariants` VALUES (20, 'Phone', 'Samsung S8', 3.99, 5, '2020-03-10 01:13:45', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for providers
-- ----------------------------
DROP TABLE IF EXISTS `providers`;
CREATE TABLE `providers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `email_contact` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `link_page` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 95 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of providers
-- ----------------------------
INSERT INTO `providers` VALUES (1, 'Eqvvol official store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (2, 'hadinas Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (3, 'Vghoqget Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (4, 'Alan Family', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (5, 'keajor officialflagship Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (6, 'VOONGSON Clytze 3C Digital Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (7, 'eHong Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (8, 'Rankman Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (9, 'Opto Direct Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (10, 'Smart Tec Co.,Ltd', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (11, 'BASEUS Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (12, 'BASEUS Co.,Ltd. Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (13, 'llano Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (14, 'Adeeing Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (15, 'BASEUS OfficialFlagship Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (16, 'EECPT Direct Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (17, 'TISHRIC Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (18, 'BrandPeripherals Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (19, 'Shenzhen Linghuang store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (20, 'Selena Love DIY Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (21, 'Leslie ConsumerElectronics Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (22, 'YottaMaster Direct Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (23, 'Arealer Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (24, 'Umemory Direct NO.1', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (25, 'Rocky Revoution Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (26, 'BRANDOO TEAM Brandoo Security Eshop Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (27, 'USAMS Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (28, 'bolimei Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (29, 'Lamorniea Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (30, 'Gemnes Retail Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (31, 'PB Expert Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (32, 'SOODOO Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (33, 'DONG LAI Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (34, 'Good Wholesaler Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (35, 'Efishion Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (36, 'TD mobuy wishes trading', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (37, 'LinMai Good Friend Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (38, 'USB Flash Drive Factory + 1 Year Warranty', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (39, 'FUNM\'OB Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (40, 'XNARKRA Trendoriginal Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (41, 'used to livinG Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (42, 'XNARKRA Genuineoriginal Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (43, 'DETAILS Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (44, 'Top1 Phone Holder Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (45, 'MobileMate Arena', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (46, 'BIBOVI Phone Accessories Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (47, 'ZLNHIV Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (48, 'Jerefish Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (49, 'anew Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (50, 'Wandering Earths Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (51, 'ICEbear Official Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (52, 'Shop5083102 Store', 'Null', '', '2020-03-06 02:23:28', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (53, 'KUEGOU Speciality Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (54, 'Shop5008183 Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (55, 'Shop4993328 Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (56, 'Shop4033013 Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (57, 'XUZIGANGGE Speciality Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (58, 'Liseaven Official Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (59, 'Shop4258015 Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (60, 'li fen wen na Official Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (61, 'fuzhuangz Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (62, 'FH Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (63, 'fashionable 2019 Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (64, 'VOLGINS Official Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (65, 'Shop4476060 Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (66, 'DAYUHU GoodQuality Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (67, 'Customize Shirts&Hoodies(Drop Shipping Service ) Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (68, 'Evening trend Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (69, 'PRIVATHINKER SINICISM Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (70, 'KuKu Boys Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (71, 'FunkyBeauty Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (72, 'Shop4924029 Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (73, 'ZOGGA CLOTH Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (74, 'Shop2807066 Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (75, 'GYMOHYEAH Official Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (76, 'Kpop Bag Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (77, 'Mayma Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (78, 'YEMEKE Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (79, 'THE COOLMIND Official Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (80, 'Cool Men Apparel', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (81, 'Shop4963049 Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (82, 'Muscleguys Official Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (83, 'VagaryTees Official Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (84, 'WXDUUZ duoduo Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (85, 'JALEE Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (86, 'Miss Charm', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (87, 'Vomint Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (88, 'SHANBAO Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (89, 'Male elves Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (90, '365 Buy Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (91, 'oOIOo Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (92, 'BIANYILONG Official Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (93, 'Apparel China', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `providers` VALUES (94, 'MRMT Lovepure Store', 'Null', '', '2020-03-06 02:23:29', 1, '0000-00-00 00:00:00', 0, 0);

-- ----------------------------
-- Table structure for servicepolices
-- ----------------------------
DROP TABLE IF EXISTS `servicepolices`;
CREATE TABLE `servicepolices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `strong` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `index_position` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of servicepolices
-- ----------------------------
INSERT INTO `servicepolices` VALUES (1, 'from $50', 'Free Delivery', 'ec-transport', 2, '2020-03-04 18:32:36', 1, NULL, NULL, 0);
INSERT INTO `servicepolices` VALUES (2, 'Feedbacks', '99% Positive', 'ec-customers', 2, '2020-03-04 18:32:36', 1, NULL, NULL, 0);
INSERT INTO `servicepolices` VALUES (3, 'for free return', '365 days', 'ec-returning', 2, '2020-03-04 18:32:36', 1, NULL, NULL, 0);
INSERT INTO `servicepolices` VALUES (4, 'Secure System', 'Payment', 'ec-payment', 2, '2020-03-04 18:32:36', 1, NULL, NULL, 0);
INSERT INTO `servicepolices` VALUES (5, 'Brands', 'Only Best', 'ec-tag', 2, '2020-03-04 18:32:36', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for states
-- ----------------------------
DROP TABLE IF EXISTS `states`;
CREATE TABLE `states`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `countryservice_id` int(11) NULL DEFAULT NULL,
  `available` tinyint(1) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE,
  INDEX `countryservice_id`(`countryservice_id`) USING BTREE,
  CONSTRAINT `states_ibfk_1` FOREIGN KEY (`countryservice_id`) REFERENCES `countryservices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of states
-- ----------------------------

-- ----------------------------
-- Table structure for stateservices
-- ----------------------------
DROP TABLE IF EXISTS `stateservices`;
CREATE TABLE `stateservices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `available` tinyint(1) NULL DEFAULT NULL,
  `countryservice_id` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE,
  INDEX `countryservice_id`(`countryservice_id`) USING BTREE,
  CONSTRAINT `stateservices_ibfk_1` FOREIGN KEY (`countryservice_id`) REFERENCES `countryservices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stateservices
-- ----------------------------
INSERT INTO `stateservices` VALUES (1, 'Alabama', 'AL', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (2, 'Alaska', 'AK', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (3, 'American Samoa', 'AS', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (4, 'Arizona', 'AZ', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (5, 'Arkansas', 'AR', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (6, 'California', 'CA', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (7, 'Colorado', 'CO', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (8, 'Connecticut', 'CT', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (9, 'Delaware', 'DE', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (10, 'District Of Columbia', 'DC', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (11, 'Federated States Of Micronesia', 'FM', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (12, 'Florida', 'FL', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (13, 'Georgia', 'GA', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (14, 'Guam', 'GU', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (15, 'Hawaii', 'HI', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (16, 'Idaho', 'ID', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (17, 'Illinois', 'IL', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (18, 'Indiana', 'IN', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (19, 'Iowa', 'IA', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (20, 'Kansas', 'KS', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (21, 'Kentucky', 'KY', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (22, 'Louisiana', 'LA', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (23, 'Maine', 'ME', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (24, 'Marshall Islands', 'MH', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (25, 'Maryland', 'MD', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (26, 'Massachusetts', 'MA', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (27, 'Michigan', 'MI', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (28, 'Minnesota', 'MN', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (29, 'Mississippi', 'MS', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (30, 'Missouri', 'MO', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (31, 'Montana', 'MT', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (32, 'Nebraska', 'NE', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (33, 'Nevada', 'NV', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (34, 'New Hampshire', 'NH', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (35, 'New Jersey', 'NJ', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (36, 'New Mexico', 'NM', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (37, 'New York', 'NY', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (38, 'North Carolina', 'NC', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (39, 'North Dakota', 'ND', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (40, 'Northern Mariana Islands', 'MP', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (41, 'Ohio', 'OH', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (42, 'Oklahoma', 'OK', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (43, 'Oregon', 'OR', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (44, 'Palau', 'PW', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (45, 'Pennsylvania', 'PA', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (46, 'Puerto Rico', 'PR', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (47, 'Rhode Island', 'RI', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (48, 'South Carolina', 'SC', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (49, 'South Dakota', 'SD', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (50, 'Tennessee', 'TN', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (51, 'Texas', 'TX', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (52, 'Utah', 'UT', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (53, 'Vermont', 'VT', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (54, 'Virgin Islands', 'VI', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (55, 'Virginia', 'VA', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (56, 'Washington', 'WA', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (57, 'West Virginia', 'WV', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (58, 'Wisconsin', 'WI', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);
INSERT INTO `stateservices` VALUES (59, 'Wyoming', 'WY', 0, 232, '2020-03-04 18:30:40', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `color` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of status
-- ----------------------------

-- ----------------------------
-- Table structure for tabcarouselproducts
-- ----------------------------
DROP TABLE IF EXISTS `tabcarouselproducts`;
CREATE TABLE `tabcarouselproducts`  (
  `tabcarousel_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  INDEX `tabcarousel_id`(`tabcarousel_id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `tabcarouselproducts_ibfk_1` FOREIGN KEY (`tabcarousel_id`) REFERENCES `tabscarousel` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tabcarouselproducts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tabcarouselproducts
-- ----------------------------
INSERT INTO `tabcarouselproducts` VALUES (1, 15);
INSERT INTO `tabcarouselproducts` VALUES (1, 39);
INSERT INTO `tabcarouselproducts` VALUES (1, 85);
INSERT INTO `tabcarouselproducts` VALUES (1, 24);
INSERT INTO `tabcarouselproducts` VALUES (1, 65);
INSERT INTO `tabcarouselproducts` VALUES (2, 77);
INSERT INTO `tabcarouselproducts` VALUES (2, 35);
INSERT INTO `tabcarouselproducts` VALUES (2, 14);
INSERT INTO `tabcarouselproducts` VALUES (2, 69);
INSERT INTO `tabcarouselproducts` VALUES (2, 19);
INSERT INTO `tabcarouselproducts` VALUES (3, 11);
INSERT INTO `tabcarouselproducts` VALUES (3, 53);
INSERT INTO `tabcarouselproducts` VALUES (3, 61);
INSERT INTO `tabcarouselproducts` VALUES (3, 20);
INSERT INTO `tabcarouselproducts` VALUES (3, 7);

-- ----------------------------
-- Table structure for tabscarousel
-- ----------------------------
DROP TABLE IF EXISTS `tabscarousel`;
CREATE TABLE `tabscarousel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tab_text` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `index_position` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tabscarousel
-- ----------------------------
INSERT INTO `tabscarousel` VALUES (1, 'Featured', 1, '2020-03-06 00:06:09', 1, NULL, NULL, 0);
INSERT INTO `tabscarousel` VALUES (2, 'On Sale', 1, '2020-03-06 00:06:28', 1, NULL, NULL, 0);
INSERT INTO `tabscarousel` VALUES (3, 'Top Reated', 1, '2020-03-06 00:06:28', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for trendingproducts
-- ----------------------------
DROP TABLE IF EXISTS `trendingproducts`;
CREATE TABLE `trendingproducts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `view_times` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `index_position` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `trendingproducts_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trendingproducts
-- ----------------------------
INSERT INTO `trendingproducts` VALUES (1, 4, 5, 1, '2020-03-04 15:30:02', 1, NULL, NULL, 0);
INSERT INTO `trendingproducts` VALUES (2, 45, 29, 1, '2020-03-04 15:30:02', 1, NULL, NULL, 0);
INSERT INTO `trendingproducts` VALUES (3, 73, 12, 1, '2020-03-04 15:30:02', 1, NULL, NULL, 0);
INSERT INTO `trendingproducts` VALUES (4, 88, 39, 1, '2020-03-04 15:30:02', 1, NULL, NULL, 0);
INSERT INTO `trendingproducts` VALUES (5, 120, 77, 1, '2020-03-04 15:30:02', 1, NULL, NULL, 0);

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of types
-- ----------------------------

-- ----------------------------
-- Table structure for userprofiles
-- ----------------------------
DROP TABLE IF EXISTS `userprofiles`;
CREATE TABLE `userprofiles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `lastname` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `age` datetime(0) NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `userprofiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userprofiles
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `password` varchar(94) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `cover_image` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `create` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------

-- ----------------------------
-- Table structure for usertypes
-- ----------------------------
DROP TABLE IF EXISTS `usertypes`;
CREATE TABLE `usertypes`  (
  `user_id` int(11) NULL DEFAULT NULL,
  `type_id` int(11) NULL DEFAULT NULL,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `type_id`(`type_id`) USING BTREE,
  CONSTRAINT `usertypes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `usertypes_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usertypes
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
