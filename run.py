from app import app
from models import db
from app.functions import mail

if __name__ == '__main__':
	mail.init_app(app)
	db.init_app(app)

	with app.app_context():
		db.create_all()

	app.run(port=3000)