import os
import stripe
from flask import Flask
from config import DevelopmentConfig

app = Flask(__name__)

stripe_keys = {
  'secret_key_stripe': os.environ['STRIPE_PUBLISHABLE_KEY'],
  'publishable_key_stripe': os.environ['STRIPE_SECRET_KEY']
}

stripe.api_key = stripe_keys['secret_key_stripe']

app.config.from_object(DevelopmentConfig)

from app import views
