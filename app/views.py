import os
from flask import request, redirect, render_template, url_for, jsonify, json, make_response, flash
from app import app, forms, functions, custom_functions
from models import db, Product, Provider, Category, ListCharacteristic, ProductVariant, CountryService, StateService, Department, OwlCarousel, ServicePolice, MiniPromo, TabCarousel, BigCarousel, CarouselCategory, TrendingProduct, BestSeller, TabCarouselProduct, BigTabCarouselProduct, BestSellerProduct, User, UserProfile
from sqlalchemy.orm import load_only, joinedload
from collections import defaultdict
from json import encoder
import zipcodes
import datetime
encoder.FLOAT_REPR = lambda o: format(o, '.2f')

# choose navbar style
FullColorNavbar = 1
OriginalNavbar = 2

# Choose body style
FullHome = 'home page-template page-template-template-homepage-v3 full-color-background'
FullSingle = 'single-product full-width'
FullHomeDefault = 'page home page-template-default'

# Main page
@app.route('/')
def index():
	# here we get all the departments
	depart_query = Department.query.all()
	# here we get all the categories
	cate_query = Category.query.all()

	index_data = []

	# Here we have to make all the index queries
	owl_query = functions.select_specific_query(OwlCarousel, [OwlCarousel.title, OwlCarousel.subtitle, OwlCarousel.strong, OwlCarousel.img, OwlCarousel.link, OwlCarousel.button_text], [OwlCarousel.deleted == 0])
	
	if len(owl_query) != 0:
		index_data.append({"Owl Carousel":owl_query})
	
	service_query = functions.select_specific_query(ServicePolice, [ServicePolice.text, ServicePolice.strong, ServicePolice.icon], [ServicePolice.deleted == 0])
	
	if len(service_query) != 0:
		index_data.append({"Service Polices":service_query})
	
	mini_query = functions.select_specific_query(MiniPromo, [MiniPromo.text, MiniPromo.strong, MiniPromo.img, MiniPromo.size, MiniPromo.link, MiniPromo.button_text], [MiniPromo.deleted == 0])
	
	if len(mini_query) != 0:
		index_data.append({"Mini Promo":mini_query})

	carousel_query = functions.select_specific_query(TabCarousel, [TabCarousel.tab_text, Product.id, Product.name, Product.price, Product.discount_price, Product.cover_image, Category.name], [TabCarousel.deleted == 0, Product.deleted == 0, Category.deleted ==0], [TabCarouselProduct, Product, Category], TabCarousel.tab_text)

	if len(carousel_query) != 0:		
		index_data.append(custom_functions.orderData("Carousel Product", carousel_query))

	big_query = functions.select_specific_query(BigCarousel, [BigCarousel.text, BigCarousel.main_img, Product.id, Product.name, Product.price, Product.discount_price, Product.cover_image, Category.name], [BigCarousel.deleted == 0, Product.deleted == 0, Category.deleted == 0], [BigTabCarouselProduct, Product, Category])	

	if len(big_query) != 0:
		index_data.append(custom_functions.orderData("Big Carousel", big_query))

	carocategory_query = functions.select_specific_query(CarouselCategory, [Category.name, Product.id, Product.name, Product.price, Product.cover_image, CarouselCategory.index_position], [CarouselCategory.deleted == 0, Category.deleted == 0, Product.deleted == 0], [Category, Product])
	
	if len(big_query) != 0:
		index_data.append(custom_functions.orderData("Category Carousel", carocategory_query))

	trending_query = functions.select_specific_query(TrendingProduct, [Product.id, Product.name, Product.price, Product.cover_image, Category.name, TrendingProduct.index_position], [TrendingProduct.deleted == 0, Product.deleted == 0, Category.deleted == 0], [Product, Category], TrendingProduct.view_times)

	if len(big_query) != 0:
		index_data.append(custom_functions.orderData("Trending", trending_query))

	best_query = functions.select_specific_query(BestSeller, [BestSeller.tab_text, BestSeller.main_product, BestSeller.index_position, Product.id, Product.name, Product.price, Product.discount_price, Product.cover_image, Category.name], [BestSeller.deleted == 0, Product.deleted == 0, Category.deleted == 0], [BestSellerProduct, Product, Category], Product.sold_times)

	if len(best_query) != 0:
		index_data.append(custom_functions.orderData("BestSeller", best_query))

	topcate_query = functions.select_specific_query(Department, [Department.name, Department.img, Category.name], [Department.deleted == 0, Category.deleted == 0], [Category], Department.view_times)

	if len(topcate_query) != 0:
		index_data.append(custom_functions.orderData("TopCategory", topcate_query))	

	return render_template('index.html.j2', title = 'All Store', body_style = FullHome ,navbarstyle = FullColorNavbar, categories = cate_query, departments = depart_query, data = index_data)

@app.route('/Manage Products', methods = ['GET', 'POST'])
def manageproducts():

	cate_query = Category.query.all()

	depart_query = Department.query.all()

	uploadfile = forms.UploadFiles()

	if request.method == 'POST':
		# check if the post request has the file part
		if 'file' not in request.files:
			return redirect(request.url)

		file = request.files['file']

		# if user does not select file, browser also
		# submit an empty part without filename
		if file.filename == '':
			return redirect(request.url)

		custom_functions.load_data_from_file(file)

	return render_template('manage_products.html.j2', title = 'Manage Products', body_style = FullHome ,navbarstyle = OriginalNavbar, form = uploadfile, departments = depart_query ,categories = cate_query)

@app.route('/Products', methods = ['POST','GET'])
@app.route('/Products/<int:page>', methods = ['POST','GET'])
@app.route('/Products/<filterBy>', methods = ['POST','GET'])
@app.route('/Products/<filterBy>/<int:page>', methods = ['POST','GET'])
def products(filterBy = "Sale", page = 1):	

	# Load all categories
	cate_query = Category.query.all()
	# Load all departments
	depart_query = Department.query.all()
	# Basic columns
	Columns = [Product.id, Product.name, Product.price, Product.discount_price, Product.description, Product.cover_image, Product.shipping_days, Product.star_rating, Category.name]
	# Default items per page 25
	per_page = 20
	# Load filter options
	if ( functions.validate_exist_register(Department, [Department.name == filterBy], 'Single') ) == True:
		filter_options = functions.select_specific_query(ProductVariant, [Product.id ,ProductVariant.variant, ProductVariant.value], [ProductVariant.deleted == 0, Department.name == filterBy], [Product, Category, Department])
	elif ( functions.validate_exist_register(Category, [Category.name == filterBy], 'Single') ) == True:
		filter_options = functions.select_specific_query(ProductVariant, [Product.id ,ProductVariant.variant, ProductVariant.value], [ProductVariant.deleted == 0, Category.name == filterBy], [Product, Category])
	else:
		filter_options = []

	# Order filters data
	if len(filter_options) != 0:
		filters = custom_functions.orderData("Filter Options", filter_options)
	else:
		filters = None

	# Validate how many products per page
	if "show" in request.form.keys():
		per_page = int(request.form['show'])

	if filterBy == "Sale":
		products_query = functions.Select_paginate(Product, Columns, None ,[Category], page, per_page)
	else:
		if 'amount' in request.form.keys() and request.form['amount'] is not None:

			filterPrice = request.form['amount'].replace("$", "", 2).replace(" ", "", 2).split("-")

			if ( functions.validate_exist_register(Department, [Department.name == filterBy], 'Single') ) == True:
				products_query = functions.Select_paginate(Product, Columns, [Department.name == filterBy, Category.department_id == Department.id, Product.price >= filterPrice[0], Product.price <= filterPrice[1], Product.deleted == 0, Category.deleted == 0], [Category, Department], page, per_page)
			# Validate if filter is category
			elif ( functions.validate_exist_register(Category, [Category.name == filterBy], 'Single') ) == True:
				products_query = functions.Select_paginate(Product, Columns, [Category.name == filterBy, Product.price >= filterPrice[0], Product.price <= filterPrice[1], Product.deleted == 0, Category.deleted == 0], [Category], page, per_page)
			elif ( functions.validate_exist_register(ProductVariant, [ProductVariant.value == filterBy], 'Single' ) ) == True:
				products_query = functions.Select_paginate(Product, Columns, [ProductVariant.value == filterBy, Product.deleted == 0, Category.deleted == 0, ProductVariant.deleted == 0], [Category, ProductVariant], page, per_page)
		elif "orderby" in request.form.keys():
			order = request.form['orderby']			
			if order == "popularity":
				if ( functions.validate_exist_register(Category, [Category.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(TrendingProduct, Columns, [TrendingProduct.deleted == 0, Product.deleted == 0, Category.deleted == 0, Category.name == filterBy], [Product, Category], page, per_page, [TrendingProduct.view_times], "asc")
				elif ( functions.validate_exist_register(Department, [Department.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(TrendingProduct, Columns, [TrendingProduct.deleted == 0, Product.deleted == 0, Category.deleted == 0, Department.name == filterBy], [Product, Category, Department], page, per_page, [TrendingProduct.view_times], "asc")
			elif order == "rating":
				if ( functions.validate_exist_register(Category, [Category.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(Product, Columns, [Product.deleted == 0, Category.deleted == 0, Category.name == filterBy], [Category], page, per_page, [Product.star_rating], "desc")
				elif ( functions.validate_exist_register(Department, [Department.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(Product, Columns, [Product.deleted == 0, Category.deleted == 0, Department.name == filterBy], [Category, Department], page, per_page, [Product.star_rating], "desc")
			elif order == "date":
				if ( functions.validate_exist_register(Category, [Category.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(Product, Columns, [Product.deleted == 0, Category.deleted == 0, Category.name == filterBy], [Category], page, per_page, [Product.create], "desc")
				elif ( functions.validate_exist_register(Department, [Department.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(Product, Columns, [Product.deleted == 0, Category.deleted == 0, Department.name == filterBy], [Category, Department], page, per_page, [Product.create], "desc")
			elif order == "price":
				if ( functions.validate_exist_register(Category, [Category.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(Product, Columns, [Product.deleted == 0, Category.deleted == 0, Category.name == filterBy], [Category], page, per_page, [Product.price], "asc")
				elif ( functions.validate_exist_register(Department, [Department.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(Product, Columns, [Product.deleted == 0, Category.deleted == 0, Department.name == filterBy], [Category, Department], page, per_page, [Product.price], "asc")
			elif order == "price-desc":
				if ( functions.validate_exist_register(Category, [Category.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(Product, Columns, [Product.deleted == 0, Category.deleted == 0, Category.name == filterBy], [Category], page, per_page, [Product.price], "desc")
				elif ( functions.validate_exist_register(Department, [Department.name == filterBy] , 'Single') ) == True:
					products_query = functions.Select_paginate(Product, Columns, [Product.deleted == 0, Category.deleted == 0, Department.name == filterBy], [Category, Department], page, per_page, [Product.price], "desc")
		else:
			if ( functions.validate_exist_register(Department, [Department.name == filterBy], 'Single') ) == True:

				products_query = functions.Select_paginate(Product, Columns, [Department.name == filterBy, Category.department_id == Department.id, Product.deleted == 0, Category.deleted == 0], [Category, Department], page, per_page)
			
			elif ( functions.validate_exist_register(Category, [Category.name == filterBy], 'Single') ) == True:

				products_query = functions.Select_paginate(Product, Columns, [Category.name == filterBy, Product.deleted == 0, Category.deleted == 0], [Category], page, per_page)

			elif ( functions.validate_exist_register(ProductVariant, [ProductVariant.value == filterBy], 'Single' ) ) == True:

				products_query = functions.Select_paginate(Product, Columns, [ProductVariant.value == filterBy, Product.deleted == 0, Category.deleted == 0, ProductVariant.deleted == 0], [Category, ProductVariant], page, per_page)

		# Validate what the filter is
		# if ( functions.validate_exist_register(Department, [Department.name == filterBy], 'Single') ) == True:
		# 	products_query = functions.Select_paginate(Product, Columns, [Department.name == filterBy, Category.department_id == Department.id, Product.price >= filterPrice[0], Product.price <= filterPrice[1], Product.deleted == 0, Category.deleted == 0], [Category, Department], page, per_page)
		# # Validate if filter is category
		# elif ( functions.validate_exist_register(Category, [Category.name == filterBy], 'Single') ) == True:
		# 	products_query = functions.Select_paginate(Product, Columns, [Category.name == filterBy, Product.price >= filterPrice[0], Product.price <= filterPrice[1], Product.deleted == 0, Category.deleted == 0], [Category], page, per_page)
		# elif ( functions.validate_exist_register(ProductVariant, [ProductVariant.value == filterBy], 'Single' ) ) == True:
		# 	products_query = functions.Select_paginate(Product, Columns, [ProductVariant.value == filterBy, Product.deleted == 0, Category.deleted == 0, ProductVariant.deleted == 0], [Category, ProductVariant], page, per_page)


	return render_template('products.html.j2', title = 'Products', body_style = FullHome ,navbarstyle = OriginalNavbar, departments = depart_query ,categories = cate_query, products = products_query, url_pagination = filterBy, filters_options = filters)

@app.route('/Details/<int:product_id>', methods = ['POST', 'GET'])
def details(product_id):

	Columns = [Product.id, Product.name, Product.price, Product.discount_price, Product.description, Product.cover_image, Product.shipping_days, Product.star_rating, Category.name]

	product_details_query = functions.select_specific_query(Product, Columns, [Product.deleted != 1, Product.id == product_id, Product.deleted != 1], [Category])

	folderName = product_details_query[0][5].split("/cover")

	product_details_img = functions.SearchFolder(folderName[0], 'cover'+folderName[1]);

	description_list = functions.select_specific_query(ListCharacteristic,['characteristic'], [ListCharacteristic.product_id == product_id, ListCharacteristic.deleted != 1])

	product_variants = functions.select_specific_query(ProductVariant, ["variant","value","price"], [ProductVariant.product_id == product_id, ProductVariant.deleted != 1])

	variants = defaultdict(list)

	for value in product_variants:
		items = [value[1], value[2]]
		variants[value[0]].append(items)

	return render_template('details.html.j2', title = 'Details', body_style = FullSingle, navbarstyle = OriginalNavbar, product = product_details_query, product_images = product_details_img, description_list = description_list, product_variants = variants)

@app.route('/wishlist', methods = ['POST', 'GET'])
def wishlist():
	# This views is going to execute just a javascript function so i have to declarate wich functions
	return render_template('wishlist.html.j2', title = 'Wishlist', body_style = FullHomeDefault, navbarstyle = OriginalNavbar)

@app.route('/cart')
def cart():
	
	calculate = forms.CalculateShipping()

	calculate.Country.choices = [(row.code, row.name) for row in db.session.query(CountryService).filter(CountryService.available == 0)]

	return render_template('cart.html.j2', title = "Cart", body_style = FullHomeDefault, navbarstyle = OriginalNavbar, form = calculate)

@app.route("/checkout", methods = ['POST','GET'])
@app.route("/checkout/<code>/<zip>", methods = ['POST','GET'])
def checkout(code = None, zip = None):

	if code is not None and zip is not None:
		# Set variable for forms
		checkoutFields = forms.Checkout();
		# getting country's data
		countryInfo = db.session.query(CountryService).filter(CountryService.code == code).first()
		# Validate which state it is
		zipcode = zipcodes.matching(zip)

		if countryInfo != None and len(zipcode) != 0:
			# set all select options States
			checkoutFields.State.choices = [(row.code, row.name) for row in db.session.query(StateService).filter(StateService.available == 0, StateService.countryservice_id == countryInfo.id)]
			# Set default data from cart
			checkoutFields.State.data  = zipcode[0]['state']
			# Set all select option Countries
			checkoutFields.Country.choices = [(row.code, row.name) for row in db.session.query(CountryService).filter(CountryService.available == 0, CountryService.id == countryInfo.id)]
			# Set default data from cart
			checkoutFields.Country.data = code
			
			return render_template("checkout.html.j2", title = "Checkout", body_style = FullHomeDefault, navbarstyle = OriginalNavbar, form = checkoutFields, stripeKey = os.environ['STRIPE_PUBLISHABLE_KEY'])
		else:
			return redirect(url_for('cart'))
	else:
		return redirect(url_for('cart'))

@app.route("/My Account", methods = ['POST', 'GET'])
def myaccount():

	if request.form:
		if "Email" in request.form:
			email = request.form['Email']
			register_result = custom_functions.RequestRegisterUser(email)
			if register_result == True:
				flash(["message-success", "Congratulation" ,"Check your email to complete the registration"])
			else:
				flash(["message-warning", "Ops" ,"Something went wrong, please try again later."])


	LoginForm = forms.Login()

	RegisterForm = forms.RequestRegister()
	
	return render_template("login.html.j2", title = "My Account", body_style = FullHomeDefault , url_pagination = "Login", navbarstyle = OriginalNavbar, LoginForm = LoginForm, RegisterForm = RegisterForm)

@app.route("/Registration", methods = ['POST','GET'])
@app.route("/Registration/<token>", methods = ['POST','GET'])
def registration(token):

	verify_token = functions.confirm_token(token)

	registration_form = forms.Registration(request.form)

	return render_template("registration.html.j2", title = "Registration From", info = verify_token, Form = registration_form)


@app.route("/Contact Us")
def contact():
	
	return render_template("contact.html.j2", tittle = "Contact Us")

@app.route("/About Us")
def aboutus():
	
	return render_template("aboutus.html.j2", tittle = "About Us")

@app.route("/successPayment/<token>" ,methods = ['POST', 'GET'])
def successPayment(token):

	return render_template("successPayment.html.j2", tittle = "Payment Success")

# This view and function is for ajax request *CUSTOM
@app.route("/ajax-request", methods = ['POST', 'GET'])
def ajax_request():	

	# Force True will ignore the Content-type
	try:
		data = request.get_json(force=True)		
	except Exception as e:
		data = {'image' : request.files['file'],'registerform' : request.form['userdata'] }

	# Validate which funcitons is going to execute
	if "favorites" in data:	
		response = custom_functions.load_favorite_page(data["favorites"],data["columns"])
	elif "modal_cart" in data:
		response = custom_functions.show_item_cart_modal(data)
	elif "products" in data:
		response = custom_functions.show_cart_items_tag(data['products'],data['times'],["id","price"])
	elif "mini_cart" in data:
		response = custom_functions.show_mini_cart_items(data["mini_cart"], ["id","name", "price", "cover_image"])
	elif "cart" in data:
		response = custom_functions.load_cart_page(data["cart"], ["id","name", "price", "cover_image"])
	elif "checkout" in data:
		response = custom_functions.checkout_proccess(data["checkout"], data["shippingCost"], data["shippingAddress"])
	elif "confirmationToken" in data:		
		response = custom_functions.loadConfirmationData(data["confirmationToken"])
	elif "country" in data:
		response = custom_functions.CalculateShipping(data)
	elif "registerform" in data:
		response = custom_functions.RegisterUser(data)
	
	return json.dumps(response)