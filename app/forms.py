from wtforms import Form, StringField, TextField, IntegerField, SelectField, BooleanField,TextAreaField, validators, PasswordField
from wtforms.fields.html5 import EmailField
from flask_wtf.file import FileField, FileRequired
from wtforms.fields.html5 import DateField

class Login(Form):

    Username = StringField("Username *")

    Password = PasswordField("Password *")

    Remember = BooleanField("Remember me")

class RequestRegister(Form):

    Email = EmailField("Email Address *", 
            [
                validators.Email(message="Use a valid email address"),
                validators.length(min=4, max=50, message="Use a valid email address")
            ]
        )
    
class Registration(Form):

    name = StringField("First Name", 
            [
                validators.Required(message="This input is required"),
                validators.length(min=2, max=25, message="Use a valid name")
            ]
        )
    
    lastname = StringField("Last Name", 
            [
                validators.Required(message="This input is required"),
                validators.length(min=2, max=25, message="Use a valid last name")
            ]
        )

    age = DateField("Date of birth", 
            [
                validators.Required(message="This input is required")
            ]
        )

    email = StringField("Email", 
            [
                validators.Required(message="This input is required"),
                validators.Email(message="Use a valid email address"),
                validators.length(min=4, max=50, message="Use a valid email address")
            ]
        )

    username = StringField("User name", 
            [
                validators.Required(message="This input is required")
            ]
        )

    phone = StringField("Phone number", 
            [
                validators.Required(message="This input is required"),
                validators.length(min=10, max=10, message="Use a phone number valid")
            ]
        )

    profile_img = FileField('Profile Image', validators=[FileRequired()])

    password = PasswordField("Password", 
            [
                validators.Required(message="This input is required")
            ]
        )

    confirm_password = PasswordField("Confirm Password", 
            [
                validators.Required(message="This input is required")
            ]
        )


        

class UploadFiles(Form):
	file = FileField('Search File', validators=[FileRequired()])

class Checkout(Form):
    FirstName = TextField("First Name",
        [
            validators.Required(message="The first name is required"),
            validators.length(min=4, max=25, message="Use a valid name")
    ]
    )
    LastName = TextField("Last Name",
        [
            validators.Required(message="The last name is required")
        ]
    )
    Email = EmailField("Email Address", 
        [
            validators.Required(message="The email address is required"),
            validators.Email(message="Use a valid email address"),
            validators.length(min=4, max=50, message="Use a valid email address")
        ]
    )

    PhoneNumber = IntegerField("Phone", 
        [
            validators.length(min=10, max=10, message="Use a phone number valid")
        ]
    )

    Address = TextField("Address", 
        [
            validators.Required(message="The Addres is required")
        ]
    )

    AddressDescription = TextField(" ")
    
    Country = SelectField(u"Country", id="select_country")

    City = TextField("City / Town", 
        [
            validators.Required(message="The city is required")
        ]
    )

    State = SelectField(u"State", id='select_state')

    HaveAccount = BooleanField("Create an account?")

    ZipDiffeAddress = BooleanField("Ship to a different address?")

    OrderNotes = TextAreaField("Order Notes")

class CalculateShipping(Form):

    Country = SelectField(u"Country", id="select_country")

    ZipCode = IntegerField("zip")
