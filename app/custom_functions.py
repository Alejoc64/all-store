from werkzeug.utils import secure_filename
from app import app, functions
import openpyxl
import os
from datetime import datetime
from models import db, Product, Provider, Category, ListCharacteristic, ProductVariant, Order, User, UserProfile, CountryService, StateService
from flask import flash, request
from collections import Counter
import stripe
from decimal import *
import datetime
import zipcodes
from flask import request, redirect, render_template, url_for, jsonify, json, make_response, flash
# parameters for flash
# flash([kind alert, message ,full message])

def load_data_from_file(file):
	#validate if the file was uploaded
	if functions.upload_files(file, 'Products Files') == True:
		# get the file location
		location = os.environ['UPLOAD_FOLDER'] + '/' + file.filename

		# if os.path.exists(location) != True:
		# set up the stay to use openpyxl
		wb_obj = openpyxl.load_workbook(location)

		sheet_obj = wb_obj.active
		# get max columns
		number_columns = sheet_obj.max_column + 1
		# get max rows plus 1 cause add the last one
		number_rows = sheet_obj.max_row
		# set the start row
		start_row = 2
		# split columns to put on the talbe products
		required_columns_products = [1,3,4,5,6,7,8,9,10]
		# split columns to put on the talbe provider
		required_columns_providers = [2]

		# to save the processed data
		data_product = {}
		data_provider = {}

		# Getting data and colum names
		while start_row < number_rows:
			# this is to move through the columns
			for i in range(1,number_columns):
				# Get data from actual position
				cell_obj = sheet_obj.cell(row = start_row, column = i)
				# Get column name
				column_name = sheet_obj.cell(row = 1, column = i)
				# Validate if any column is empty
				if cell_obj.value != None:
					# Validate
					if i in required_columns_products:
						# save data
						data_product[column_name.value] = (cell_obj.value)
						# data_product[column_name.value] = functions.clean_special_characters(cell_obj.value)
						if i == 4:
							data_product["discount_price"] = 0
						elif i == 6:
							data_product["star_rating"] = 5
						elif i == 8:
							data_product["provider_id"] = ''
					elif i in required_columns_providers:
						# save data
						data_provider[column_name.value] = (cell_obj.value)
						# data_provider[column_name.value] = functions.clean_special_characters(cell_obj.value)
						if i == 2:
							data_provider['email_contact'] = "Null"
							data_provider['link_page'] = ""
				else:
					flash(["message-warning", "Ops" ,"We Could't upload file.", "Your file doesn't have the correct info in {}".format(column_name.value)])
					return

			try:
				# validate is the register exist
				if functions.validate_exist_register(Provider,{'name': data_provider['name']}, "Single") == False:
					# Add provider data
					provider = Provider(**data_provider)
					db.session.add(provider)
					db.session.commit()

				# get info about provider
				provider_data = Provider.query.filter_by(name = data_provider['name']).first()


				# validate is the register exist
				if functions.validate_exist_register(Provider, {'name': data_product['name'], 'provider_id': provider_data.id}, "Join", Product) == False:
					#Get category ID
					category_data = Category.query.filter_by(name = data_product['category_id']).first()
					# Validate
					if category_data is not None:
						# add Provider_id to complete product info
						data_product['provider_id'] = provider_data.id
						data_product['category_id'] = category_data.id
						# add product info
						product = Product(**data_product)
						db.session.add(product)
						db.session.commit()
					else:
						# This error means teh categori doesn't exist in database
						flash(["message-warning", "Ops" ,"We Could't upload file. Your file doesn't have the correct info in category_id"])
						break

				start_row = start_row + 1
			except KeyError as err:
				flash(["message-warning", "Ops" ,"We Could't upload file. There's something wrong in your file.", "Check your file and make sure everything is right. 301"])
				return
			except AttributeError as err:
				#
				flash(["message-warning", "Ops" ,"We Could't upload file. There's something wrong in your file.", "Check your file and make sure everything is right. 302"])
				return
			except TypeError as err:
				# This error is cause there's a column that is not on the columns
				flash(["message-warning", "Ops {0}".format(err) ,"We Could't upload file. There's something wrong in your file.", "Check your file and make sure everything is right. 303"])
				return

		if start_row >= number_rows:
			flash(["message-success", "Congratulation" ,"Success upload file."])
	else:
		flash(["message-danger", "Ops" ,"We Could't upload file"])

# Favorite page function
def load_favorite_page(json_items, json_columns):
	
	query = []
	response = []
	# Make a for to get all item's data
	for item in json_items:
		product = functions.select_specific_query(Product, json_columns, [Product.id == item, Product.deleted != 1])
		query.append(product)
	# getting better the data
	for product in query:
		for attr in product:
			product_data = []
			for x in attr:
				if isinstance(x, str) == False:
					x = float(x)
				product_data.append(x)
			response.append(product_data)

	return response

def show_item_cart_modal(product_id):
	
	response = []

	query = functions.select_specific_query(Product,['id','name','price','cover_image'], [Product.id == product_id['modal_cart']])

	for product in query:		
		for attr in product:
			if isinstance(attr, str) == False:
				attr = float(attr)
			response.append(attr)

	return response

def show_cart_items_tag(products_id, times, columns):

	response = 0

	query = functions.select_specific_query(Product, columns, [Product.id == products_id])

	for product in query:
		response = product[1] * times
	
	return float(response)

# def show_cart_items_tag(products_id, columns):	
# 	response = 0
# 	# Get how many times are the same product
# 	count_item_repeat = dict(Counter(products_id))
# 	# Get the item price
# 	query = functions.select_specific_query(Product, columns, [Product.id.in_(products_id)])	
# 	# I go through all the items and make the sum
# 	for product in query:
# 		# I'm validaring if the product is in the dict
# 		if str(product[0]) in count_item_repeat.keys():
# 			# I multitly the item price by how manys times it is
# 			response = response + (product[1] * count_item_repeat[str(product[0])])	
# 	return float(response)

def show_mini_cart_items(products_id, columns):
	query = functions.select_specific_query(Product, columns, [Product.id.in_(products_id)])
	# query = functions.select_specific_query(Product, columns, [Product.id == products_id])
	response = []

	for product in query:
		product_data = []
		for attr in product:
			if isinstance(attr, Decimal) == True:
				attr = float(attr)
			product_data.append(attr)
		response.append(product_data)

	return response

def load_cart_page(products, columns):
	query = functions.select_specific_query(Product, columns, [Product.id.in_(products)])

	response = []

	for product in query:
		product_data = []
		for attr in product:
			if isinstance(attr, Decimal) == True:
				attr = float(attr)
			product_data.append(attr)
		response.append(product_data)

	return response

def checkout_proccess(data, shippingCost, shippingTo):

	itemsId = []
	amount = 0
	stripe.api_key = os.environ['STRIPE_SECRET_KEY']

	for item in data:
		itemsId.append(item['id'])
	
	query = functions.select_specific_query(Product, ['id', 'price'], [Product.id.in_(itemsId)])

	for products in query:
		for items in data:
			if products.id == items['id']:
				if products.price != 0 and items['qty'] != 0:
					amount += products.price * items['qty']
	

	intentAmount = str(round(float(amount) + float(shippingCost), 2))

	intentAmount = intentAmount.replace(".","")

	intent = stripe.PaymentIntent.create(
	  amount= int(intentAmount),
	  currency='usd',
	)

	# Create data to inser in dataBase
	data = [1,6, shippingTo.replace(" ","-") ,functions.CalculateArrive(15).strftime("%Y-%m-%d"), amount, intent["created"]]

	# Create in dataBase
	insertQuery = functions.InsertData(data);
	
	if insertQuery is True:
		response = intent
	else:
		response = False

	return response

def loadConfirmationData(data):
	response = []

	query = functions.select_specific_query(Order, [Order.shipping_address,Order.estimated_arrive,Order.total_amount,Order.created_code,UserProfile.name, UserProfile.lastname], [Order.created_code == data, Order.deleted != 1], [User,UserProfile])	

	for x in query[0]:
		if isinstance(x, Decimal) == True:
			x = float(x)
		elif isinstance(x, datetime.date) == True:
			x = x.strftime("%b %d %Y")
		elif x.find("-") != -1:
			x = x.replace("-", " ")			
		response.append(x)
		
	return response

def CalculateShipping(data):
	validateZip = zipcodes.matching(data['zipcode'])

	try:
		if validateZip and validateZip[0]["country"] == data["country"]:
			query = functions.select_specific_query(CountryService, ['shipping_price'], [CountryService.code == data['country'], CountryService.deleted != 1])
			
			response = float(query[0].shipping_price)
		else:
			response = "Error"
	except UnboundLocalError as e:
		print(e)

	return response

def orderData(option, data):
	
	response = {}

	if option == "Carousel Product":
		response[option] = {}
		for items in data:
			info = []			
			for key, value in enumerate(items):
				if key != 0:
					info.append(value)
			
			if len(response[option]) == 0:				
				response[option] = {items.tab_text : [info]}
			elif items.tab_text in response[option].keys():				
				response[option][items.tab_text].append(info)				
			else:				
				response[option][items.tab_text] = [info]
	elif option == "Big Carousel":
		response[option] = []
		for items in data:
			info = []
			name = ""
			img = ""
			for key, value in enumerate(items):
				if key == 0:
					name = value
				elif key == 1:
					img = value
				else:
					info.append(value)
						
			if len(response[option]) == 0:
				addStuff = [name,img,[info]]
				response[option].append(addStuff)
			elif len(response[option]) >= 1:
				for times in response[option]:
					if times[0] == name:
						times[2].append(info)
	elif option == "Category Carousel":
		response[option] = {}
		position = "first"
		for item in data:
			info = []
			name = ""
			groupCount = 1
			for key, value in enumerate(item):
				if key == 0:
					name = value
				elif key != 5:
					info.append(value)

			if position == "first":
				info.append(position)
				position = "last"
			elif position == "last":
				info.append(position)
				position = "first"

			if len(response[option]) == 0:
				response[option][name] = {groupCount:[info]}
			else:
				if len(response[option][name][groupCount]) < 4:
					response[option][name][groupCount].append(info)
				else:
					groupCount += 1
					response[option][name][groupCount] = [info]

	elif option == "Trending":
		response[option] = {}
		position = "first"
		for item in data:
			info = []
			groupCount = 1
			for key, value in enumerate(item):
				info.append(value)

			if position == "first":
				info.append(position)
				position = "last"
			elif position == "last":
				info.append(position)
				position = "first"

			if len(response[option]) == 0:
				response[option] = {groupCount:[info]}
			else:				
				if len(response[option][groupCount]) < 4:
					response[option][groupCount].append(info)
				else:
					groupCount += 1
					response[option][groupCount] = [info]
	elif option == "BestSeller":
		response[option] = {}
		for items in data:
			info = []
			main_product = []
			tab_name = ""			
			for index, value in enumerate(items):
				if index == 0:
					tab_name = value
				elif index == 1:

					main_product = functions.select_specific_query(Product, [Product.id, Product.name, Product.price, Product.discount_price, Product.cover_image, Category.name], [Product.id == value, Product.deleted == 0, Category.deleted == 0], [Category])

					product_data = main_product[0]

					folderName = main_product[0][4].split("/cover")
					
					product_details_img = functions.SearchFolder(folderName[0], 'cover'+folderName[1])

					main_data = {"Product" : product_data, "Product_images" : product_details_img}

				else:
					info.append(value)

			if len(response[option]) == 0:
				response[option][tab_name] = { "Products" : [info], "Main" : main_data }
			elif tab_name not in response[option].keys():
				response[option][tab_name] = { "Products" : [info], "Main" : main_data }
			
			if tab_name in response[option].keys() and info not in response[option][tab_name]["Products"]:
				response[option][tab_name]["Products"].append(info)
	elif option == "TopCategory":
		response[option] = {}
		depart_count = 1		
		for items in data:
			
			if len(response[option]) == 0:
				response[option][items[0]] = {"Depart_img" : items[1], "Categories" : [ ] }

			if items[0] in response[option].keys():
				response[option][items[0]]["Categories"].append(items[2])
			else:
				response[option][items[0]] = {"Depart_img" : items[1], "Categories" : [items[2]]}
	elif option == "Filter Options":
		filters = ""		
		opt_counts = 1
		product = 0
		for items in data:				
			filters = items.variant
			if len(response) == 0:
				response[filters] = {}
				response[filters]["show"] = {items.value : opt_counts }
				product = items.id
			elif filters in response.keys():
				if items.value in response[filters]["show"].keys():
					if items.id != product:
						opt_counts +=1
						response[filters].update({items.value : opt_counts})
				elif len(response[filters]["show"]) <= 3:
					opt_counts = 1
					response[filters]["show"].update({ items.value : opt_counts })
				elif "hide" in response[filters].keys():
					if items.value in response[filters]["hide"].keys():
						if items.id != product:
							opt_counts +=1
							response[filters]["hide"].update({items.value : opt_counts})
					else:
						opt_counts = 1
						if items.value not in response[filters]["show"].keys():
							response[filters]["hide"].update({ items.value : opt_counts })

				elif "hide" not in response[filters].keys():
					response[filters]["hide"] = {items.value : opt_counts}
			else:
				opt_counts = 1
				response.update( { filters : {"show" : { items.value : opt_counts } } } )

	return response

def RequestRegisterUser(email):
	if functions.validate_exist_register(UserProfile, [email == email], "Single") == False:
		token = functions.generate_confirmation_token(email)
		confirm_url = url_for('registration', token = token, _external = True)
		response = functions.SendEmail(email, confirm_url)
	else:
		flash(["message-warning", "Ops" ,"This email is already assigned to another account"])
		response = False

	return response

def RegisterUser(data):

	originData = list(data['registerform'].split("&"))

	userData = []

	for index, value in enumerate(originData):
		startposition = value.find('=') + 1
		value = value[startposition:]
		
		if index == 3:
			value = functions.clean_special_characters(value).replace(' ', '')
			print(value)
		
		userData.append(value)
		
	if functions.validate_exist_register(UserProfile, [UserProfile.email == userData[7]], 'Single') == False:
		if functions.validate_exist_register(UserProfile, [User.username == userData[4]], 'Single') == False:
			
			if userData[5] == userData[6]:

				# Set data for model
				user = User(userData[4],
							userData[5])

				# submit to database
				db.session.add(user)
				db.session.commit()

				#Get user id
				userinfo = functions.select_specific_query(User, [User.id], [User.username == userData[4]])


				if 'image' in data:
					file = data['image']
					# Load file to folder
					if functions.upload_files(file, "User " + str(userinfo[0].id)) == True:
						profile_img = os.environ['UPLOAD_FOLDER'] + '/' + "User " + str(userinfo[0].id) + '/' + file.filename
					elif functions.upload_files(file, "User " + str(userinfo[0].id)) == False:
						flash(["message-danger", "Ops" ,"We Could't upload file"])
						profile_img = "Null"				
				else:			
					profile_img = "Null"

				userprofile = UserProfile(									
								userData[0],
								userData[1],
								profile_img,
								userData[2],
								userData[7],
								userData[3],
								userinfo[0].id,
								userinfo[0].id
							)

				db.session.add(userprofile)
				db.session.commit()

				response = {'Name' : userData[0] + " " + userData[1], 'Profile': profile_img, 'Username' : userData[4]}

			else:
				flash(["message-warning", "Password error" ,"The password doesn't match."])
				response = False
		else:
			flash(["message-warning", "Username error" ,"The username is already associated with another account."])
			response = False
	else:
		flash(["message-warning", "Email error" ,"The email is already associated with another account."])
		response = False

	return response	