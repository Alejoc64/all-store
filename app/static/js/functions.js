////// ************** GLOBAL VARIABLES ************* ////////
var GlobalApp = new Vue({
    el: '#page',
    data: {
      itemsCart: 0,
      totalCartPrice: 0,
      miniCartInfo: {},
      qtyAdd: 1,
      wishList:[],
      response: [],
      showModal: undefined, //Most to be false
      cartBag:[],
      addCart: false,
      countryCode: undefined,
      countryZip: undefined,
      countryPrice: undefined,
      paymentMethod: false,
      termsCondition: false,
      stripe: Stripe('pk_test_y2jeuXIEf6sPe68qLlk2Qr6A00uUPYtLtg'),
      card: "",
      modalContent: 
      [
        {
          modalTitle: [],
          modalBody: "",
          modalFooter: "",
          modalButton: []
        }
      ],
      checkoutFields: 
      [
        {
            customerName: "",
            customerLast: "",
            customerEmail: "",
            customerPhone: "",
            customerAddress: "",
            customerAddressDes: "",
            customerCity: "",
            customerState: "",
        }
      ]
  },
    methods: {
        startUpVar() {                            
        let loadWishList = JSON.parse(localStorage.getItem("favorite"));
        let loadCart = JSON.parse(localStorage.getItem("cart"));
        let countryChoose = JSON.parse(localStorage.getItem("country"))

        if(loadWishList != null)
            this.wishList = loadWishList

        if(loadCart != null)
            this.cartBag = loadCart

        if(countryChoose != null){          
          this.countryCode = countryChoose[0].code;
          this.countryZip = countryChoose[0].zip;
          this.countryPrice = countryChoose[0].price;
        }            
        },
        addToCart(product) {
          axios.post('/ajax-request',{
            modal_cart: product
          }).then(res => {
            if(!this.cartBag.some(item => item.id == product)) {
              this.modalContent.modalTitle = 
              [
                {
                  text: "You added this item to the cart",
                  class: "bg-info"
                }
              ];
              this.modalContent.modalBody = '<table style="margin-bottom:0px"><tr><td> <img width="100" src="/static/'+ res.data[3] +'"> </td><td>'+ res.data[1] +'</td><td>$ '+ res.data[2].toFixed(2) +'</td></tr></table>';
              this.modalContent.modalButton = 
              [
                {
                  text: 'Continue Shopping',
                  class: 'btn btn-secondary',
                  col: 'col-md-6',
                  click: "addToLocalCart",
                  args: ["close"]
                },
                {
                  text: 'Go to cart',
                  class: 'btn btn-primary',
                  col: 'col-md-6',
                  click: "addToLocalCart",
                  args: ["url", "/cart"]
                }
              ];
              this.cartBag.push({id: res.data[0],qty: parseInt(this.qtyAdd)});
              this.addToLocalCart("New", res.data[0])
            } else {
              this.modalContent.modalTitle = 
              [
                {
                  text: "Wait!",
                  class: "bg-warning"
                }
              ];
              this.modalContent.modalBody = 'You already added this item to your cart. Do you want to add again?';
              this.modalContent.modalButton = 
              [
                {
                  text: 'Cancel',
                  class: 'btn btn-secondary',
                  col: 'col-md-6',
                  click: 'addToLocalCart',
                  args: ["close"]
                },
                {
                  text: 'Add it',
                  class: 'btn btn-primary',
                  col: 'col-md-6',
                  click: 'addToLocalCart',
                  args: ["Old", res.data[0]]
                }
              ];
            }
            this.showModal = "block";
          }).catch(err => {
            console.log(err)
          });
        },
        call(functionname, args = []) {
          this[functionname](...args)
        },
        addToLocalCart(option, data) {
          switch (option) {
            case "New":
              localStorage.setItem('cart', JSON.stringify(this.cartBag));
              break;
            case "Old":
              this.cartBag.some(item => {
                if(item.id == data)
                  item.qty = item.qty + parseInt(this.qtyAdd);
              });
              localStorage.setItem('cart', JSON.stringify(this.cartBag));
              this.showModal = "none";
              axios.post('/ajax-request',{
                modal_cart: data
              }).then(res => {
                this.modalContent.modalTitle = 
              [
                {
                  text: "You added this item to the cart",
                  class: "bg-info"
                }
              ];
              this.modalContent.modalBody = '<table style="margin-bottom:0px"><tr><td> <img width="100" src="/static/'+ res.data[3] +'"> </td><td>'+ res.data[1] +'</td><td>$ '+ res.data[2].toFixed(2) +'</td></tr></table>';
              this.modalContent.modalButton = 
              [
                {
                  text: 'Continue Shopping',
                  class: 'btn btn-secondary',
                  col: 'col-md-6',
                  click: "",
                },
                {
                  text: 'Go to cart',
                  class: 'btn btn-primary',
                  col: 'col-md-6',
                  click: "",
                }
              ];
              this.showModal = "block";
              }).catch(err => {
                console.log('Paila')
              });
              break;
            case "url":
                window.location.replace(data);
                break;
            default:
              this.showModal = "none";
              break;
          }
        },
        addToWishlist(product) {
          if (product != null)
            (!this.wishList.includes(product)) ? this.wishList.push(product) : this.wishList.splice(this.wishList.indexOf(product), 1);
          localStorage.setItem('favorite', JSON.stringify(this.wishList));     
      },
      deleteItemCart(index) {
        for(value in this.miniCartInfo) {
          if(this.miniCartInfo[value].includes(index)){
            this.miniCartInfo.splice(value, 1);
            for(item in this.cartBag){
              if(this.cartBag[item].id == index){                
                this.cartBag.splice(item, 1);
              }
            }
          }
        }
        localStorage.setItem('cart', JSON.stringify(this.cartBag));
      },
      deleteWishlist(index) {
        for(key in this.wishList) {
          if(this.wishList[key] == index)
            this.wishList.splice(key, 1);
        }
        localStorage.setItem('favorite', JSON.stringify(this.wishList));
      },
      updateCart(item, action) {        
        let tmp = this.cartBag.find(element => element.id == item)
        let position = this.cartBag.indexOf(tmp)
        switch (action) {
          case '+':
            tmp.qty++;
            break;
          case '-':
            tmp.qty--;
            break;
        }
        this.cartBag.splice(position, 1)
        this.cartBag.push(tmp)
        localStorage.setItem('cart', JSON.stringify(this.cartBag));
      },
      calculate_shipping() {
        axios.post('/ajax-request', {
          country: this.countryCode,
          zipcode: this.countryZip
        }).then(res => {
          if (res.data != "Error"){
            $("#zipError").text("");
            this.countryPrice = res.data
            CountryPackage = [{'code' : this.countryCode, 'zip' : this.countryZip, 'price' : this.countryPrice}]
            localStorage.setItem('country', JSON.stringify(CountryPackage));
          } else if (res.data == "Error")
            $("#zipError").text("Sorry, this zip code isn't validate");
        }).catch(err => {
          console.log(err)
        });
      },
      inputMask() {
        let key = event.key;
        let allowCommant = ['Backspace', 'ArrowLeft', 'ArrowRight', 'Tab']

        if(!allowCommant.includes(key)){
          switch (event.target.id) {
            // case "CardNumber":
            //   if(/^\d+/g.test(key)){
            //     if(currentValue.length == 4 || currentValue.length == 9 || currentValue.length == 14)
            //       event.target.value = currentValue + " ";
            //     if(event.target.value.length <= 18)
            //       event.target.value += key;
            //   } else 
            //     event.preventDefault();
            //   break;
            // case "CVV":
            //   if(/^\d+/g.test(key)){
            //     if(event.target.value.length <= 3)
            //         event.target.value += key;
            //     else
            //       event.preventDefault();
            //   }
            //   break;
            // case "ExpirationDate":
            //   if(/^\d+/g.test(key)){
            //     if(currentValue.length < 5){
            //       if(currentValue.length == 2)
            //         event.target.value = currentValue + "/" + key;
            //       else
            //         event.target.value += key;
            //     }
            //   }
            //   break;
            case "ZipCode":
                  if(!/^\d+/g.test(key) || this.countryZip.length >= 5)
                    event.preventDefault();
              break;
          }
        }
      },
      createCheckoutForm() {

        var elements = this.stripe.elements();

        var style = {
          base: {
            color: "#32325d",
          }
        };

        this.card = elements.create("card", { style: style });
        this.card.mount("#card-element");

        this.card.addEventListener('change', ({error}) => {
          const displayError = document.getElementById('card-errors');
          if (error) {
            displayError.textContent = error.message;
          } else {
            displayError.textContent = '';
          }
        });
      },
      sendCheckout(){

        let shippingTo = this.checkoutFields.customerAddress +" "+ this.checkoutFields.customerAddressDes +" "+ this.checkoutFields.customerCity +" "+ this.checkoutFields.customerState +" "+ this.countryZip;

        axios.post('/ajax-request',{
            checkout: this.cartBag,
            shippingCost: this.countryPrice,
            shippingAddress: shippingTo
        }).then(res => {
            this.stripe.confirmCardPayment(res.data.client_secret, {
                payment_method: {
                    card: this.card,
                    billing_details: {
                        name: this.checkoutFields.customerName + " " + this.checkoutFields.customerLast,
                        address: shippingTo,
                        email: this.checkoutFields.customerEmail,
                        phone: this.checkoutFields.customerPhone,
                    }
                }
            }).then(function(result) {
                if (result.error) {
                    // Show error to your customer (e.g., insufficient funds)
                    $("#card-errors").text('* '+result.error.message)
                } else {
                    // The payment has been processed!
                    if (result.paymentIntent.status === 'succeeded') {
                        this.cartBag = []
                        localStorage.removeItem("cart");
                        localStorage.removeItem("country");
                        window.location.replace("/successPayment/" + res.data.created);
                    }
                }
            });
        }).catch(err => {
            console.log(err)
        });
      },
      calculateVariant() {
        $('.electro-price .amount').text(event.target.value);
      },
      ClearVariant() {
        $('#variantSelect').prop('selectedIndex',0);        
        $('.electro-price .amount').text($('#variantSelect').val());
      },
      AddQty() {
        var currentAmount = $('#variantSelect').val();
        if(currentAmount == undefined) {
          currentAmount = $('.electro-price .amount').text();
        }        
        var newAmount = currentAmount * event.target.value;
        $('.electro-price .amount').text((newAmount).toFixed(2));
      }
    },
    computed: {
      getCartInfo() {
        if (Object.entries(this.cartBag).length != 0){
          let idCartItems = []
          this.itemsCart = 0;
          this.totalCartPrice = 0;
          this.cartBag.some(item => {
            idCartItems.push(item.id)
            // request info to item tag and total price
            axios.post('/ajax-request', {
              products: item.id,
              times: item.qty
            }).then(res => {
              this.totalCartPrice += res.data
            }).catch(err => {
              console.log("Paila mijo ome")
            });
            this.itemsCart += item.qty
          });
         // request to load info on mini cart
          axios.post('/ajax-request', {
            mini_cart: idCartItems
          }).then(res => {            
            for(item of res.data){
              this.cartBag.some(itemBag => {
                if(itemBag.id == item[0])
                  item.push(itemBag.qty)
              });              
            }
            this.miniCartInfo = res.data
          }).catch(err => {
            console.log('Error Paila')
          });
          this.countryUpdate = JSON.parse(localStorage.getItem("country"))
        } else {
          this.totalCartPrice = 0;
          this.itemsCart = 0;
        }
      },
      loadWishListPage() {
        if(this.wishList.length != 0){
          axios.post('/ajax-request', {
            favorites: this.wishList,
            columns: ["id", "name", "price", "cover_image"]
          }).then(res => {
            this.response = res.data
          }).catch(err => {
            this.response = ""
          });
        } else {
          this.response = ""
        }
      },
      loadCartPage() {
        if (Object.entries(this.cartBag).length > 0){
          let itemsId = []
          this.cartBag.some(item => {
            itemsId.push(item.id)
          });
          axios.post('/ajax-request', {
            cart: itemsId
          }).then(res => {
            for (item of res.data){
              this.cartBag.some(value => {
                if(value.id == item[0])
                  item.push(value.qty)
              })
            }            
            this.response = res.data
          }).catch(err => {
            console.log('Paila load page cart')
          });          
        } else {
          this.response = ""
        }
      },
      loadCheckoutPage() {
        if(this.countryUpdate != null){
          let itemsId = []
          this.cartBag.some(item => {
            itemsId.push(item.id)
          });
          axios.post('/ajax-request', {
            cart: itemsId
          }).then(res => {
            for (item of res.data){
              this.cartBag.some(value => {
                if(value.id == item[0])
                  item.push(value.qty)
              })
            }            
            this.response = res.data
          }).catch(err => {
            console.log('Paila Mijo')
          });
        }
      },
    },
    beforeMount() {
      this.startUpVar();
    },
    delimiters: ['[[', ']]']
});