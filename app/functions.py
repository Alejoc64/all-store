
import glob, os, random 
from werkzeug.utils import secure_filename
from app import app
import openpyxl
import os, re
from datetime import datetime, timedelta
from models import db, Product, Provider, Category, Order
from sqlalchemy import desc, asc
from sqlalchemy.orm import load_only
from flask import flash, json
from cryptography.fernet import Fernet
from flask_mail import Mail, Message
from flask import request, redirect, render_template, url_for, jsonify, json, make_response, flash
from itsdangerous import URLSafeTimedSerializer

# Set up global vars
mail = Mail() 

# parameters for flash
# flash([kind alert, message ,full message])

def allowed_file(file):
	# split name an extension and check if the extension is on the list
	return '.' in file and file.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

def upload_files(file, folder = None):	

	try:
		#validate if the extension is allowed
		if file and allowed_file(file.filename):
			# Get the file name
			filename = secure_filename(file.filename)
			# check if the file was saved
			file.save(os.path.join(os.environ['UPLOAD_FOLDER'] + '/' + folder, filename))		
			return True
		else:			
			return False
	except FileNotFoundError as e:
		try:
			os.mkdir(os.environ['UPLOAD_FOLDER'] + '/' + folder)
			upload_files(file, folder)
			return True
		except OSError as e:
			flash(["message-danger", "Ops" ,"We Could't upload file"])			
			return False

def select_specific_query(Model, Columns, Filter = None, Join = None, Order = None):

	if Model is not None and Columns is not None:
		if Filter is not None and Join is None:			
			response = Model.query.filter(*Filter).with_entities(*Columns).order_by(Order).all()
		elif Filter is not None and Join is not None:
			response = Model.query.join(*Join).filter(*Filter).with_entities(*Columns).order_by(Order).all()
		elif Filter is None and Join is not None:			
			response = Model.query.join(Join).with_entities(*Columns).order_by(Order).all()
		elif Filter is None and Join is None:			
			response = Model.query.with_entities(*Columns).order_by(Order).all()

	return response


def Select_paginate(Model, Columns, Filter = None, Join = None, page = 1, per_page = 35, ColumOrder = [], Order = None):

	if len(ColumOrder) == 0:
		ColumOrder = Model.id
	else:
		ColumOrder = ColumOrder[0]

	if Order == "asc" or Order == None:
		OrderBy = ColumOrder.asc()
	elif Order == "desc":
		OrderBy = ColumOrder.desc()

	if Model is not None and Columns is not None:
		if Filter is not None and Join is None:
			response = Model.query.filter(*Filter).with_entities(*Columns).order_by(OrderBy).paginate(page, per_page, False)
		elif Filter is not None and Join is not None:
			response = Model.query.join(*Join).filter(*Filter).with_entities(*Columns).order_by(OrderBy).paginate(page, per_page, False)
		elif Filter is None and Join is not None:
			response = Model.query.join(*Join).with_entities(*Columns).order_by(OrderBy).paginate(page, per_page, False)
		elif Filter is None and Join is None:
			response = Model.query.with_entities(*Columns).order_by(OrderBy).paginate(page, per_page, False)
		return response

# This function consult the tables an verify if the register exist
def validate_exist_register(Model_one, Filter, Type, Model_two = "None"):
	# validate type of consult
	if Type == 'Single':
		query = db.session.query(Model_one).filter(*Filter).first()
	elif Type == 'Join':
		if Model_two != "None":
			query = db.session.query(Model_one).join(Model_two).filter_by(*Filter).first()

	if query is not None:
		return True
	else:
		return False
# This function clean data depent what i want
def clean_special_characters(string,option = None):
	response = ''
	if isinstance(string, str) == True:
		for character in string.split("/n"):
			tmp = re.sub(r"[^a-zA-Z0-9]+", ' ', character)
			response = response + tmp
	elif isinstance(string, int) == True:
		if string.isdigit() == True:
			response = string
		else:
			return False
	elif option == "character":
		for digit in str(string):
			if digit.isalnum() == True:
				response = response + digit

	return response

def CalculateArrive(Days):
	currentDate = datetime.now()

	response = currentDate + timedelta(days=Days)

	# return response.strftime("%m/%d/%Y %H:%M:%S")
	return response

def InsertData(data):
	insertStatus = True
	updateData = Order(*data)
	try:
		db.session.add(updateData)
		db.session.commit()		
	except Exception as e:
		print(e)
		insertStatus = False

	return insertStatus

def EncryptDecrypt(data, action):
	data = clean_special_characters(data[0], "character")
	key = Fernet.generate_key()
	f = Fernet(key)
	data = data.encode()

	if action is "encrypt":
		serializer = f.encrypt(data)
	elif action is "decrypt":
		serializer = f.encrypt(data[0])

	return serializer

def SearchFolder(folderName, avoid = None):
	
	allow = list(app.config['ALLOWED_EXTENSIONS'])

	files = []

	response = []

	for file in os.listdir('app/static/'+folderName):
		if file.endswith(tuple(allow)):			
			files.append(folderName + '/' + file)

	if avoid != None:
		for file in files:
			if file != (folderName + '/' + avoid):
				response.append(file)
	else:
		response = files

	return response

def aleatorios(quantity):
	
	numbers_list = random.sample(range(0,10), quantity)
	numbers = ''.join(str(n) for n in numbers_list)
	return numbers


def generate_confirmation_token(data):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])

    return serializer.dumps(data, salt = app.config['SECURITY_PASSWORD_SALT'])

def confirm_token(token, expiration=3600):
	serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])

	try:
		response = serializer.loads(
		        token,
		        salt=app.config['SECURITY_PASSWORD_SALT'],
		        max_age=expiration
		    )
	except:
		response = False

	return response

def SendEmail(To, link = None):

	msg = Message(
			'Thanks for join us',
			sender = 'Realm Fly',
			recipients = [To]
		)

	msg.html = render_template('email_template.html', User = To, link = link)

	try:
		mail.send(msg)
		result = True
	except Exception as e:
		result = False

	return result