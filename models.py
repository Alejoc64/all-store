from flask_sqlalchemy import SQLAlchemy
import datetime
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash

db = SQLAlchemy()

UserType = db.Table('usertypes', db.metadata,
	db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
	db.Column('type_id', db.Integer, db.ForeignKey('types.id'))
	)

TabCarouselProduct = db.Table('tabcarouselproducts', db.metadata,
	db.Column('tabcarousel_id', db.Integer, db.ForeignKey('tabscarousel.id')),
	db.Column('product_id', db.Integer, db.ForeignKey('products.id'))
	)

BigTabCarouselProduct = db.Table('bigcarouselproducts', db.metadata,
	db.Column('bigcarousel_id', db.Integer, db.ForeignKey('bigcarousels.id')),
	db.Column('product_id', db.Integer, db.ForeignKey('products.id'))
	)

IndexCategoryProduct = db.Table('indexcategoryproducts', db.metadata,
	db.Column('indexcategory_id', db.Integer, db.ForeignKey('carouselcategories.id')),
	db.Column('product_id', db.Integer, db.ForeignKey('products.id'))
	)

BestSellerProduct = db.Table('bestsellersproduts', db.metadata,
	db.Column('bestseller_id', db.Integer, db.ForeignKey('bestsellers.id')),
	db.Column('product_id', db.Integer, db.ForeignKey('products.id'))
	)

class User(db.Model):
	__tablename__ = 'users'

	id = db.Column(db.Integer, primary_key = True)
	username = db.Column(db.String(250), unique = True)
	password = db.Column(db.String(94))	
	# Relationship	
	user_profile = db.relationship('UserProfile')
	order_id = db.relationship('Order')
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, username, password):
		self.username = username
		self.password = self.__create_password(password)		

	def __create_password(self, password):
		return generate_password_hash(password)

class UserProfile(db.Model):
	__tablename__ = 'userprofiles'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(250), nullable = False)
	lastname = db.Column(db.String(250), nullable = False)
	cover_image = db.Column(db.String(150))
	age = db.Column(db.DateTime)
	email = db.Column(db.String(50))
	phone = db.Column(db.Integer)
	# Relationship
	user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, name, lastname, cover_image, age, email, phone, user_id, create_by):
		self.name = name
		self.lastname = lastname
		self.cover_image = cover_image
		self.age = age
		self.email = email
		self.phone = phone
		self.user_id = user_id
		self.create_by = create_by

class Type(db.Model):
	__tablename__ = 'types'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(50))
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, name):
		self.name = name

class Order(db.Model):
	__tablename__ = 'orders'

	id = db.Column(db.Integer, primary_key = True)	
	shipping_address = db.Column(db.String(250))
	estimated_arrive = db.Column(db.DateTime)
	total_amount = db.Column(db.Numeric(8,2), nullable = False)
	created_code = db.Column(db.String(250))
	# Relationship
	user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	status = db.Column(db.Integer, db.ForeignKey('status.id'))
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, user_id, status, shipping_address,estimated_arrive, total_amount, created_code):
		self.user_id = user_id
		self.status = status
		self.shipping_address = shipping_address
		self.estimated_arrive = estimated_arrive
		self.total_amount = total_amount
		self.created_code = created_code

class Status(db.Model):
	__tablename__ = 'status'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(250))
	color = db.Column(db.String(250))
	# Relationship
	order_id = db.relationship('Order')
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, name, color):
		self.name = name
		self.color = color

class Department(db.Model):
	__tablename__ = "departments"

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(250), unique = True)
	img = db.Column(db.String(250), unique = True)
	view_times = db.Column(db.Integer)
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, name):
		self.name = name

class Category(db.Model):
	__tablename__ = 'categories'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(250))
	img = db.Column(db.String(250), unique = True)
	view_times = db.Column(db.Integer)
	# Relationship
	department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))
	product_id = db.relationship('Product')
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, name):
		self.name = name

class Product(db.Model):
	__tablename__ = 'products'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(250), unique = True, nullable = False)
	provider_price  = db.Column(db.Numeric(5,2), nullable = False)
	price = db.Column(db.Numeric(5,2), nullable = False)
	discount_price = db.Column(db.Numeric(5,2), default = 0)
	description = db.Column(db.String(250), nullable = False)
	cover_image = db.Column(db.String(150), nullable = False)
	star_rating = db.Column(db.Numeric(5,1))
	shipping_days = db.Column(db.String(50))
	shipping_price = db.Column(db.Numeric(5,2))
	link_product = db.Column(db.String(500), unique = True)
	sold_times = db.Column(db.Integer, default = 0)

	# Relationship
	provider_id = db.Column(db.Integer, db.ForeignKey('providers.id'))
	category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))	
	listcharacteristic_id = db.relationship('ListCharacteristic')
	productvariant_id = db.relationship('ProductVariant')
	trendingproduct_id = db.relationship('TrendingProduct')
	
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, name, provider_price, price, discount_price, description, cover_image, star_rating, shipping_days, shipping_price, link_product,provider_id, category_id):
		self.name = name
		self.provider_price = provider_price
		self.price = price
		self.discount_price = discount_price
		self.description = description
		self.cover_image = cover_image
		self.star_rating = star_rating
		self.shipping_days = shipping_days
		self.shipping_price = shipping_price
		self.link_product = link_product
		self.provider_id = provider_id
		self.category_id = category_id

class Provider(db.Model):
	__tablename__ = 'providers'

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(250), unique = True)
	# email_contact = db.Column(db.String(50), unique = True, default = 'Empty')
	email_contact = db.Column(db.String(50), default = '')
	link_page = db.Column(db.String(500), default = "")
	# Relationship
	product_id = db.relationship('Product')
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, name, email_contact, link_page):
		self.name = name
		self.email_contact = email_contact
		self.link_page = link_page

class ProductVariant(db.Model):
	__tablename__ = 'productvariants'

	id = db.Column(db.Integer, primary_key = True)
	variant = db.Column(db.String(50))
	value = db.Column(db.String(50))
	price = db.Column(db.Numeric(5,2))
	# Relationship
	product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, variant, value, price):
		self.variant = variant
		self.value = value
		self.price = price
		self.product_id = product_id

class ListCharacteristic(db.Model):
	__tablename__ = 'listcharacteristics'

	id = db.Column(db.Integer, primary_key = True)
	characteristic = db.Column(db.String(100))
	# Relationship
	product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, id, characteristic, product_id):
		self.id = id
		self.characteristic = characteristic
		self.product_id = product_id

class CountryService(db.Model):
	__tablename__ = "countryservices"

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(250), unique = True)
	code = db.Column(db.String(10), unique = True)
	shipping_price = db.Column(db.Numeric(5,2))
	available = db.Column(db.Boolean)
	# Relationship
	state_id = db.relationship('StateService')
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, name, code, shipping_price, available):
		self.name = name
		self.code = code
		self.shipping_price = shipping_price
		self.available = available

class StateService(db.Model):
	__tablename__ = "stateservices"

	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(250), unique = True)
	code = db.Column(db.String(10), unique = True)
	available = db.Column(db.Boolean)
	# Relationship
	countryservice_id = db.Column(db.Integer, db.ForeignKey('countryservices.id'))
	# This columns are standard for all the tables
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, name, code, countryservice_id, available):
		self.name = name
		self.code = code
		self.countryservice_id = countryservice_id
		self.available = available

class OwlCarousel(db.Model):
	__tablename__ = "owlcarousels"

	id = db.Column(db.Integer, primary_key = True)
	title = db.Column(db.String(150))
	subtitle = db.Column(db.String(150))
	strong = db.Column(db.String(150))
	img = db.Column(db.String(150))
	link = db.Column(db.String(150))
	button_text = db.Column(db.String(150))
	# This columns are standard for all the tables
	index_position = db.Column(db.Integer)
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, title, subtitle, strong, img, link, button_text):
		self.title = title
		self.subtitle = subtitle
		self.strong = strong
		self.img = img
		self.link = link
		self.button_text = button_text

class ServicePolice(db.Model):
	__tablename__ = "servicepolices"
		
	id = db.Column(db.Integer, primary_key = True)
	text = db.Column(db.String(150))
	strong = db.Column(db.String(150))
	icon = db.Column(db.String(50))
	# This columns are standard for all the tables
	index_position = db.Column(db.Integer)
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, text, strong, icon, size):
		self.text = text
		self.strong = strong
		self.icon = icon
		self.size = size

class MiniPromo(db.Model):
	__tablename__ = "minipromos"
	
	id = db.Column(db.Integer, primary_key = True)
	text = db.Column(db.String(150))
	strong = db.Column(db.String(150))
	img = db.Column(db.String(150))
	size = db.Column(db.String(150))
	link = db.Column(db.String(150))
	button_text = db.Column(db.String(150))
	# This columns are standard for all the tables
	index_position = db.Column(db.Integer)
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, text, strong, img, size, link, button_text):
		self.text = text
		self.strong = strong
		self.img = img
		self.size = size
		self.link = link
		self.button_text = button_text

class TabCarousel(db.Model):
	__tablename__ = "tabscarousel"
	
	id = db.Column(db.Integer, primary_key = True)
	tab_text = db.Column(db.String(150))
	# This columns are standard for all the tables
	index_position = db.Column(db.Integer)
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, tab_text, product_id):
		self.tab_text = tab_text
		self.product_id

class BigCarousel(db.Model):
	__tablename__ = "bigcarousels"
	
	id = db.Column(db.Integer, primary_key = True)
	text = db.Column(db.String(150))
	main_img = db.Column(db.String(150))
	# This columns are standard for all the tables
	index_position = db.Column(db.Integer)
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, text, main_img, product_id):
		self.text = text
		self.main_img = main_img
		self.product_id

class CarouselCategory(db.Model):
	__tablename__ = "carouselcategories"
	
	id = db.Column(db.Integer, primary_key = True)	
	# Relationship
	category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
	# This columns are standard for all the tables
	index_position = db.Column(db.Integer)
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, title, product_id):
		self.title = title
		self.product_id

class TrendingProduct(db.Model):
	__tablename__ = "trendingproducts"
	
	id = db.Column(db.Integer, primary_key = True)
	view_times = db.Column(db.Integer)
	# Relationship
	product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
	# This columns are standard for all the tables
	index_position = db.Column(db.Integer)
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, product_id, view_times):
		self.product_id = product_id
		self.view_times = view_times

class BestSeller(db.Model):
	__tablename__ = "bestsellers"
	
	id = db.Column(db.Integer, primary_key = True)
	tab_text = db.Column(db.String(150), unique = True)
	main_product = db.Column(db.Integer, default = 0)	
	# This columns are standard for all the tables
	index_position = db.Column(db.Integer)
	create = db.Column(db.DateTime, default = datetime.datetime.now)
	create_by = db.Column(db.Integer, default = 1, nullable = False)
	modified = db.Column(db.DateTime, default = '00-00-00')
	modified_by = db.Column(db.Integer, default = 0)
	deleted = db.Column(db.Integer,default = 0, nullable = False)

	def __init__(self, tab_text, product_id, main_product):
		self.tab_text = tab_text
		self.product_id = product_id
		self.main_product = main_product

# class TopCategory(db.Model):
# 	__tablename__ = "topcategories"
	
# 	id = db.Column(db.Integer, primary_key = True)	
# 	view_times = db.Column(db.Integer)
# 	# Relationship
# 	department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))
# 	# This columns are standard for all the tables
# 	index_position = db.Column(db.Integer)
# 	create = db.Column(db.DateTime, default = datetime.datetime.now)
# 	create_by = db.Column(db.Integer, default = 1, nullable = False)
# 	modified = db.Column(db.DateTime, default = '00-00-00')
# 	modified_by = db.Column(db.Integer, default = 0)
# 	deleted = db.Column(db.Integer,default = 0, nullable = False)

# 	def __init__(self, department_id, product_id, view_times):
# 		self.department_id = department_id
# 		self.product_id = product_id
# 		self.view_times = view_times